/**
*	Library for web services HTML pages
*   v 0.0.1
**/

const b64toBlob = (b64Data, contentType='', sliceSize=512) => {
	const byteCharacters = atob(b64Data);
	const byteArrays = [];

	for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
		const slice = byteCharacters.slice(offset, offset + sliceSize);

		const byteNumbers = new Array(slice.length);
		for (let i = 0; i < slice.length; i++) {
		byteNumbers[i] = slice.charCodeAt(i);
		}

		const byteArray = new Uint8Array(byteNumbers);
		byteArrays.push(byteArray);
	}

	const blob = new Blob(byteArrays, {type: contentType});
	return blob;
}

function processResult(result) {
	if (result.status === true) {
		// making downloadable link
		console.log(result.command);
		console.log(result);
		let blobx;
		if (result.format === "xlsx") {
			blobx = b64toBlob(result.data, 'application/octet-stream');
			// const blobx = URL.createObjectURL(blob);				}
			// blobx = new Blob([result.data], { type: 'application/octet-stream' }); // ! Blob
		} else {
			blobx = new Blob([result.data], { type: 'text/plain' }); // ! Blob
		}
		var url = window.URL.createObjectURL(blobx);
		var a = document.createElement('a');
		a.href = url;
		// Give filename you wish to download
		a.download = result.resultname;
		a.style.display = 'none';
		document.body.appendChild(a);
		a.click();
		a.remove();
		window.URL.revokeObjectURL(url);
	} else {
		console.log(result);
		if (result.returnvar === 5)
			alert("Le fichier à traiter n'existe pas.")
		else if (result.returnvar === 4)
			alert("Le tier principal choisi n'existe pas dans ce fichier.")
		else if (result.returnvar === 3)
			alert("Mauvaise option de la commande.")
			else if (result.returnvar === 2)
			alert("Erreur dans les parametres de la commande.")
		else if (result.returnvar === 6)
			alert("Le fichier ELAN ne semble pas être dans un format correspondant au travail demandé. Etes-vous sûr du choix de votre fichier ?")
		else
			alert("Cas d'erreur inconnu: " + result.res + " Voir aussi la console du navigateur pour plus d'information.");
	}
}

function testFormOK(csvok) {
	let fc = document.getElementById("inputfile");
	let filechoice = fc.files;
	console.log("filechoice", filechoice);
	if (filechoice.length < 1) {
		alert("Il faut sélectionner un fichier ELAN ou CVS à traiter");
		return false;
	} else if (csvok !== undefined && document.getElementById("postaction").value == 'none') {
		if (filechoice.length > 1) {
			alert("On ne peut sélectionner qu'un seul fichier CSV.");
			return false;
		}
		if (!filechoice[0].toLowerCase().endsWith(".csv")) {
			alert("Le fichier à traiter doit être un fichier CSV (donc le nom de fichier doit se terminer par .csv).\nVotre fichier se termine par " + filechoice.substring(filechoice.length - 4));
			return false;
		}
	}  else {
		for (let i = 0; i < filechoice.length; i++) {
			if (!filechoice[i] || !filechoice[i].name || (typeof filechoice[i].name !== 'string' && !(filechoice[i].name instanceof String))) {
				alert("La variable filechoice contient une valeur incorrecte. Voir console.");
				console.log(filechoice);
				console.log("indice i = " + i);
				return false;
			}
			if (!filechoice[i].name.toLowerCase().endsWith(".eaf")) {
				alert("Le fichier à traiter doit être un fichier ELAN (donc le nom de fichier doit se terminer par .eaf).\nVotre fichier se nomme: " + filechoice[i].name);
				return false;
			}
		}
	}
	return true;
}

function startRequest(form, url) {
	let formData = new FormData(form);
	var http = new XMLHttpRequest();
	http.open('POST', url, true);
	http.onreadystatechange = function() { //Call a function when the state changes.
		if (http.readyState == 4 && http.status == 200) {
			try {
				let jsonresult = JSON.parse(http.responseText);
				// console.log(jsonresult);
				processResult(jsonresult);
			} catch (e) {
				console.log(http);
				alert('error on processing data:' + http.responseText);
			}
		}
	}
	http.send(formData);
}
