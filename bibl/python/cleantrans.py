import xml.etree.ElementTree as ET
import os
import sys, getopt

corrections = {
	"-- --": "--",
	"échap": "Echap",
	"une partie du corps": "Partie du corps",
	"Un partie du corps": "Partie du corps",
	"Participant Participant": "participant",
	"Participant Manipulation Manipulation Manipulation": "participant",
}
	
def printnotice():
	print('cleantrans.py <inputfiles ...>')
	print('    Process all files (ELAN files) to remove spaces and clean the transcriptions.')

def cleantrans_onefile(filename):
	# test if input file exists
	if (not os.path.isfile(filename)):
		print("cleantrans: the file " + filename + " does not exist")
		sys.exit(5)
	file_name, file_extension = os.path.splitext(filename)
	if (file_extension != ".eaf"):
		print('Only EAF files: ' + filename + " does not end if '.eaf'")
		return ""
	xmltree = ET.parse(filename)
	root = xmltree.getroot()
	prec = 0
	modif = False
	for slot in root.iter('ANNOTATION_VALUE'):
		val = slot.text
		if val is not None:
			cleanval = val.strip()
			if cleanval in corrections:
				cleanval = corrections[cleanval]
			if (val != cleanval):
				# print(val + " --> " + cleanval)
				slot.text = cleanval
				modif = True
	if (modif == True):
		replace_file_name = file_name + '-notcleaned.eaf.bad'
		print('The file ' + filename + " was corrected and the old file is now named " + replace_file_name)
		os.rename(filename, replace_file_name)
		xmltree.write(filename)
		return replace_file_name
	else:
		print('The file ' + filename + " does not need to be corrected")
		return ""
	
def cleantrans(argv):
	try:
		opts, args = getopt.gnu_getopt(argv,"h", ["help"])
	except getopt.GetoptError:
		printnotice()
		sys.exit(3)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			printnotice()
			sys.exit(2)
	for inputfile in args:
		cleantrans_onefile(inputfile)

if __name__ == "__main__":
   cleantrans(sys.argv[1:])
