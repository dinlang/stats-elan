import pympi
import pandas as pd
import os
import re
import traceback
import sys

#sys.path.insert(1, "/applis/toolselan/bibl/python/")
#sys.path.insert(1, "/applis/dev/toolselan/bibl/python/")
sys.path.insert(1, "/Users/cp/devlopt/elan-data-analysis/bibl/python/")

import utils as utils
import compute as compute
import cleanemptytimes as cleanemptytimes
import cleantrans as cleantrans
# import intersections as intersections

# import bibl.utils as utils
# import bibl.compute as compute
# import bibl.cleanemptytimes as cleanemptytimes
# import bibl.intersections as intersections

#get_linguistic_type_names()
#get_tier_ids_for_linguistic_type(ling_type, parent=None)
#get_tier_names()
#get_parameters_for_tier(id_tier)¶

def clean_string(str):
	if str is None:
		return "--"
	clean = str.strip()
	if clean == "":
		return "--"
	else:
		return clean


def get_annotation_data_for_tier(eaf_file, tier, eaf_end, filename):
	tiernames = eaf_file.get_tier_names()
	result = []
	for t in tiernames:
		if t == tier:
			params = eaf_file.get_parameters_for_tier(t)
			annots = eaf_file.get_annotation_data_for_tier(t)
			annots_sorted = compute.sort_and_fill(annots, eaf_end)
			for a in annots_sorted:
				dur = a[1] - a[0]
				clean_a = clean_string(a[2])
				result.append((clean_a, a[0], a[1], dur, params['TIER_ID'], params['LINGUISTIC_TYPE_REF'], params['PARTICIPANT'] if 'PARTICIPANT' in params else "unknown", filename))
	return compute.sort_second_and_third_column(result)

def get_annotation_data_for_tier_part(eaf_file, tier, eaf_end, filename):
	tiernames = eaf_file.get_tier_names()
	result = []
	for t in tiernames:
		if t.find(tier) > -1:
			params = eaf_file.get_parameters_for_tier(t)
			# print("tier: " + tier + " t: " + t + " params: " + str(params))
			annots = eaf_file.get_annotation_data_for_tier(t)
			annots_sorted = compute.sort_and_fill(annots, eaf_end)
			for a in annots_sorted:
				dur = a[1] - a[0]
				clean_a = clean_string(a[2])
				result.append((clean_a, a[0], a[1], dur, params['TIER_ID'], params['LINGUISTIC_TYPE_REF'], params['PARTICIPANT'] if 'PARTICIPANT' in params else "unknown", filename))
	return compute.sort_second_and_third_column(result)

def get_annotation_data_for_tier_expr(eaf_file, tier, eaf_end, filename):
	tiernames = eaf_file.get_tier_names()
	result = []
	for t in tiernames:
		# print("tier: " + tier + " t: " + t)
		if re.match(tier, t):
			params = eaf_file.get_parameters_for_tier(t)
			annots = eaf_file.get_annotation_data_for_tier(t)
			annots_sorted = compute.sort_and_fill(annots, eaf_end)
			for a in annots_sorted:
				dur = a[1] - a[0]
				clean_a = clean_string(a[2])
				result.append((clean_a, a[0], a[1], dur, params['TIER_ID'], params['LINGUISTIC_TYPE_REF'], params['PARTICIPANT'] if 'PARTICIPANT' in params else "unknown", filename))
	return compute.sort_second_and_third_column(result)

def get_annotation_data_for_tier_fusion(eaf_file, tiers, eaf_end, filename):
	patterns = tiers.split("|")
	if len(patterns) != 2:
		print('Bad number of patterns in fusion: ' + tiers)
		sys.exit(1)
	tiernames = eaf_file.get_tier_names()
	result_left = []
	for t in tiernames:
		# if t.find(patterns[0]) > -1:
		if re.match(patterns[0], t):
			params = eaf_file.get_parameters_for_tier(t)
			annots = eaf_file.get_annotation_data_for_tier(t)
			annots_sorted = compute.sort_and_fill(annots, eaf_end)
			for a in annots_sorted:
				dur = a[1] - a[0]
				clean_a = clean_string(a[2])
				result_left.append((clean_a, a[0], a[1], dur, params['TIER_ID'], params['LINGUISTIC_TYPE_REF'], params['PARTICIPANT'] if 'PARTICIPANT' in params else "unknown", filename))
	result_right = []
	for t in tiernames:
		# if t.find(patterns[1]) > -1:
		if re.match(patterns[1], t):
			params = eaf_file.get_parameters_for_tier(t)
			annots = eaf_file.get_annotation_data_for_tier(t)
			annots_sorted = compute.sort_and_fill(annots, eaf_end)
			for a in annots_sorted:
				dur = a[1] - a[0]
				clean_a = clean_string(a[2])
				result_right.append((clean_a, a[0], a[1], dur, params['TIER_ID'], params['LINGUISTIC_TYPE_REF'], params['PARTICIPANT'] if 'PARTICIPANT' in params else "unknown", filename))
	return compute.fusion12(result_left, result_right)

def get_annotation_data_for_type(eaf_file, typ, eaf_end, filename):
	tiernames = eaf_file.get_tier_ids_for_linguistic_type(typ)
	result = []
	for t in tiernames:
		params = eaf_file.get_parameters_for_tier(t)
		annots = eaf_file.get_annotation_data_for_tier(t)
		annots_sorted = compute.sort_and_fill(annots, eaf_end)
		for a in annots_sorted:
			# filter if not within the time limits set by the parameters
			dur = a[1] - a[0]
			clean_a = clean_string(a[2])
			result.append((clean_a, a[0], a[1], dur, params['TIER_ID'], params['LINGUISTIC_TYPE_REF'], params['PARTICIPANT'] if 'PARTICIPANT' in params else "unknown", filename))
	return compute.sort_second_and_third_column(result)

def get_annotation_data_for_type_fusion_expr(eaf_file, types, eaf_end, filename):
	patterns = types.split("|")
	if len(patterns) != 2:
		print('Bad number of patterns in fusion: ' + types)
		sys.exit(1)
	typenames = eaf_file.get_linguistic_type_names()
	result_left = []
	for tt in typenames:
		# if t.find(patterns[0]) > -1:
		if re.match(patterns[0], tt):
			result_left = result_left + get_annotation_data_for_type(eaf_file, tt, eaf_end, filename)
	result_right = []
	for tt in typenames:
		# if t.find(patterns[1]) > -1:
		if re.match(patterns[1], tt):
			result_right = result_right + get_annotation_data_for_type(eaf_file, tt, eaf_end, filename)
	return compute.fusion12(result_left, result_right)

def get_annotation_data_for_type_part(eaf_file, typ, eaf_end, filename):
	types = eaf_file.get_linguistic_type_names()
	result = []
	for tt in types:
		if tt.find(typ) > -1:
			result = result + get_annotation_data_for_type(eaf_file, tt, eaf_end, filename)
	return compute.sort_second_and_third_column(result)

def get_annotation_data_for_type_expr(eaf_file, typ, eaf_end, filename):
	types = eaf_file.get_linguistic_type_names()
	result = []
	for tt in types:
		if re.match(typ, tt):
			result = result + get_annotation_data_for_type(eaf_file, tt, eaf_end, filename)
	return compute.sort_second_and_third_column(result)

def process_file(inputfile, outputfile, format, concat, set_of_annotations):
	print("calcul de " + inputfile + " et resultat dans " + outputfile)

	if len(set_of_annotations) == 1:
		df = pd.DataFrame(set_of_annotations[0], columns=['Annotation1', 'AnnotationBeginTime1', 'AnnotationEndTime1', 'AnnotationDuration1',
			'TierName1', 'TierType1', 'TierParticipant1', 'TranscriptionName'])

	if len(set_of_annotations) == 2:
		# print("set_of_annotations[0] " + str(len(set_of_annotations[0])))
		# print(set_of_annotations[0])
		# print("set_of_annotations[1] " + str(len(set_of_annotations[1])))
		# print(set_of_annotations[1])
		processed = compute.intersection2simple_slow(set_of_annotations[0], set_of_annotations[1])
		df = pd.DataFrame(processed, columns=['Annotation1', 'AnnotationBeginTime1', 'AnnotationEndTime1', 'AnnotationDuration1', 'TierName1', 'TierType1', 'TierParticipant1',
			'Annotation2', 'AnnotationBeginTime2', 'AnnotationEndTime2', 'AnnotationDuration2', 'TierName2', 'TierType2', 'TierParticipant2',
			'IntersectionBegin', 'IntersectionEnd', 'IntersectionDuration', 'TranscriptionName'])

	if len(set_of_annotations) == 3:
		processed = compute.intersection3simple(set_of_annotations[0], set_of_annotations[1], set_of_annotations[2])
		df = pd.DataFrame(processed, columns=['Annotation1', 'AnnotationBeginTime1', 'AnnotationEndTime1', 'AnnotationDuration1', 'TierName1', 'TierType1', 'TierParticipant1',
			'Annotation2', 'AnnotationBeginTime2', 'AnnotationEndTime2', 'AnnotationDuration2', 'TierName2', 'TierType2', 'TierParticipant2',
			'Annotation3', 'AnnotationBeginTime3', 'AnnotationEndTime3', 'AnnotationDuration3', 'TierName3', 'TierType3', 'TierParticipant3',
			'IntersectionBegin', 'IntersectionEnd', 'IntersectionDuration', 'TranscriptionName'])

	if len(set_of_annotations) == 4:
		processed = compute.intersection4simple(set_of_annotations[0], set_of_annotations[1], set_of_annotations[2], set_of_annotations[3])
		df = pd.DataFrame(processed, columns=['Annotation1', 'AnnotationBeginTime1', 'AnnotationEndTime1', 'AnnotationDuration1', 'TierName1', 'TierType1', 'TierParticipant1',
			'Annotation2', 'AnnotationBeginTime2', 'AnnotationEndTime2', 'AnnotationDuration2', 'TierName2', 'TierType2', 'TierParticipant2',
			'Annotation3', 'AnnotationBeginTime3', 'AnnotationEndTime3', 'AnnotationDuration3', 'TierName3', 'TierType3', 'TierParticipant3',
			'Annotation4', 'AnnotationBeginTime4', 'AnnotationEndTime4', 'AnnotationDuration4', 'TierName4', 'TierType4', 'TierParticipant4',
			'IntersectionBegin', 'IntersectionEnd', 'IntersectionDuration', 'TranscriptionName'])

	if len(set_of_annotations) == 5:
		processed = compute.intersection5simple(set_of_annotations[0], set_of_annotations[1], set_of_annotations[2], set_of_annotations[3], set_of_annotations[4])
		df = pd.DataFrame(processed, columns=['Annotation1', 'AnnotationBeginTime1', 'AnnotationEndTime1', 'AnnotationDuration1', 'TierName1', 'TierType1', 'TierParticipant1',
			'Annotation2', 'AnnotationBeginTime2', 'AnnotationEndTime2', 'AnnotationDuration2', 'TierName2', 'TierType2', 'TierParticipant2',
			'Annotation3', 'AnnotationBeginTime3', 'AnnotationEndTime3', 'AnnotationDuration3', 'TierName3', 'TierType3', 'TierParticipant3',
			'Annotation4', 'AnnotationBeginTime4', 'AnnotationEndTime4', 'AnnotationDuration4', 'TierName4', 'TierType4', 'TierParticipant4',
			'Annotation5', 'AnnotationBeginTime5', 'AnnotationEndTime5', 'AnnotationDuration5', 'TierName5', 'TierType5', 'TierParticipant5',
			'IntersectionBegin', 'IntersectionEnd', 'IntersectionDuration', 'TranscriptionName'])

	if len(set_of_annotations) > 5:
		print('NOT IMPLEMENTED')
		sys.exit(6)

	# Sauvegarde de la dataframe au format .csv
	# if file does not exist write header 
	if not concat:
		# erase outputfile
		if (os.path.isfile(outputfile)):
			os.unlink(outputfile)
		# 	# ask for erasing file ?
		if (format == "xlsx"):
			df.to_excel(outputfile, header=True, index=None)
		else:
			df.to_csv(outputfile, header='column_names', index=False)
	else: # else it exists so append without writing the header
		df.to_csv(outputfile, mode='a', header=False, index=False)

# find_elements => calcule la liste des tiers qu'il faut traiter, soit à partir des noms de tiers, ou des noms de types, ou de la modalité
# process_file => prend la liste, recupère les contenus, calcule les intersections et imprime le résultat

def find_elements(inputfile, outputfile, modality, tierstypes, format, concat):
	# print("process_file")
	# print(inputfile)
	# print(outputfile)
	# print(modality)
	# print(participant)
	# print(format)
	# print(concat)
	# test if input file exists
	if (not os.path.isfile(inputfile)):
		print("the inputfile " + inputfile + " does not exist")
		sys.exit(5)
	cleanemptytimes.cleanemptytimes([inputfile])
	cleantrans.cleantrans([inputfile])
	if (outputfile == ''):
		pathname, extension = os.path.splitext(inputfile)
		if (format == "xlsx"):
			outputfile = pathname + '.xlsx'
		else:
			outputfile = pathname + '.csv'

	# open the .eaf file
	eaf_file = pympi.Elan.Eaf(inputfile)
	# trouver tous les participants, tiers ou types
	# get last time interval 
	(eaf_start, eaf_end) = eaf_file. get_full_time_interval()

	if modality == 'visual':
		# Recuperation des annotations des pistes lng-vis-P lng-aud-P act-vis-P
		lng_vis = get_annotation_data_for_type_expr(eaf_file, '^lng-vis', eaf_end, inputfile)
		act_vis = get_annotation_data_for_type_expr(eaf_file, '^act-vis', eaf_end, inputfile)
		return process_file(inputfile, outputfile, format, concat, [lng_vis, act_vis])

	if modality == 'vocal':
		# Recuperation des annotations des pistes lng-vis-P lng-aud-P act-vis-P
		lng_vis = get_annotation_data_for_type_expr(eaf_file, '^lng-vis', eaf_end, inputfile)
		act_vis = get_annotation_data_for_type_expr(eaf_file, '^act-vis', eaf_end, inputfile)
		lng_aud = get_annotation_data_for_type_expr(eaf_file, '^lng-aud', eaf_end, inputfile)
		return process_file(inputfile, outputfile, format, concat, [lng_vis, act_vis, lng_aud])

	if modality == 'audvis':
		# Recuperation des annotations des pistes lng-vis-P lng-aud-P act-vis-P
		lng_vis = get_annotation_data_for_type_expr(eaf_file, '^lng-vis', eaf_end, inputfile)
		act_vis = get_annotation_data_for_type_expr(eaf_file, '^act-vis', eaf_end, inputfile)
		lng_aud = get_annotation_data_for_type_expr(eaf_file, '^lng-aud', eaf_end, inputfile)
		act_aud = get_annotation_data_for_type_expr(eaf_file, '^act-aud', eaf_end, inputfile)
		return process_file(inputfile, outputfile, format, concat, [lng_vis, act_vis, lng_aud, act_aud])

	all_elements = []
	for pattern in tierstypes:
		if pattern[0] == 'tier':
			if pattern[2] == 'exact':  # chercher nom exact de tier
				data = get_annotation_data_for_tier(eaf_file, pattern[1], eaf_end, inputfile)
			elif pattern[2] == 'part': # chercher partie
				data = get_annotation_data_for_tier_part(eaf_file, pattern[1], eaf_end, inputfile)
			elif pattern[2] == 'expr': # chercher expression régulière
				data = get_annotation_data_for_tier_expr(eaf_file, pattern[1], eaf_end, inputfile)
			elif pattern[2] == 'fusion': # faire fusion 2 tiers
				data = get_annotation_data_for_tier_fusion(eaf_file, pattern[1], eaf_end, inputfile)
			else:
				print('Parameter error: ' + str(pattern[1]))
				sys.exit(1)
			if data is not None:
				all_elements.append(data)
				print("Found " + str(len(data)) + " elements in " + str(pattern[1]))
		if pattern[0] == 'type':
			if pattern[2] == 'exact':  # chercher nom exact de tier
				data = get_annotation_data_for_type(eaf_file, pattern[1], eaf_end, inputfile)
			elif pattern[2] == 'part': # chercher partie
				data = get_annotation_data_for_type_part(eaf_file, pattern[1], eaf_end, inputfile)
			elif pattern[2] == 'expr': # chercher expression régulière
				data = get_annotation_data_for_type_expr(eaf_file, pattern[1], eaf_end, inputfile)
			elif pattern[1] == 'fusion': # faire fusion 2 tiers avec expr regul
				data = get_annotation_data_for_type_fusion_expr(eaf_file, pattern[1], eaf_end, inputfile)
			else:
				print('Parameter error: ' + str(pattern[1]))
				sys.exit(1)
			if data is not None:
				all_elements.append(data)
				print("Found " + str(len(data)) + " elements in " + str(pattern[1]))
	return process_file(inputfile, outputfile, format, concat, all_elements)

import sys, getopt

def printhelp():
	print('intersections-main.py <inputfiles ...> -o <outputfile> -t <type> -p <participant> -a')
	print('   inputfiles = listes des fichiers a traiter')
	print('   -o outfile = fichier resultat au format csv')
	print('   -m modality = visual => pour les familles signeuses et vocal => pour les familles entendantes, audvis pour visual and audio')
	print('         Ne pas utiliser modality si -p, --pp, --pe et/ou -t, --tp, --te sont utilisés')
	print('   -p tier = nom d\'un tier')
	print('   --pp partie_tier = partie de nom de tier')
	print('   --pe expr = recherche de tier par expression régulière (M.*, .*-vis-.*, ...)')
	print('   -t type = nom d\'un type')
	print('   --tp partie_type = partie de nom de type')
	print('   --te expr = recherche de type par expression régulière (inter.*, .*-vis.*, ...)')
	print('   -fusion patterns = deux tiers pour la fusion')
	print('   -fte patterns = deux types pour la fusion')
	print('   -f format = file output format: CSV or XLSX')
	print('   -c = concatene files. Works only with CSV extension')

def printnotice():
	print('intersections-main.py <inputfiles ...> -o <outputfile> [-m <modality> -p <participant> --pp part --pe expr -t <type> --tp part --te expr --fusion tier1|tier2 -a -f format -c]')
	print('intersections-main.py -h ; for help')

def compute_intersections(argv):
	outputfile = ''
	tierstypes = []
	modality = ''
	format = ""
	concat = False
	try:
		opts, args = getopt.gnu_getopt(argv,"m:o:p:f:cht:",["modality=","ofile=","part=", "type=", "pp=", "pe=", "tp=", "te=", "format=", "concat", "help", "fusion="])
	except getopt.GetoptError:
		printnotice()
		sys.exit(3)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			printhelp()
			sys.exit(2)
		elif opt in ("-m", "--modality"):
			if (arg != 'visual' and arg != 'vocal' and arg != 'audvis' and arg != 'all'):
				print('Error: the modality can be only visual or vocal or audvis or all, not ' + arg)
				sys.exit(2)
			modality = arg
		elif opt in ("-p", "--part"):
			tierstypes.append(('tier', arg, 'exact'))
		elif opt in ("--pp"):
			tierstypes.append(('tier', arg, 'part'))
		elif opt in ("--pe"):
			tierstypes.append(('tier', arg, 'expr'))
		elif opt in ("-t", "--type"):
			tierstypes.append(('type', arg, 'exact'))
		elif opt in ("--tp"):
			tierstypes.append(('type', arg, 'part'))
		elif opt in ("--te"):
			tierstypes.append(('type', arg, 'expr'))
		elif opt == "--fusion":
			tierstypes.append(('tier', arg, 'fusion'))
		elif opt == "--fte":
			tierstypes.append(('type', arg, 'fusion'))
		elif opt in ("-c", "--concat"):
			concat = True
		elif opt in ("-f", "--format"):
			format = arg.lower()
			if (format != "csv" and format != "xlsx"):
				print('Error: the format has to be CSV or XLSX, not ' + arg)
				sys.exit(2)
		elif opt in ("-o", "--ofile"):
			outputfile = arg
	format = utils.processargs(args, outputfile, format, printnotice)
	if (concat == True and format == "xlsx"):
		print('concatenation is only possible with CSV format')
		sys.exit(2)
	if (concat == True and outputfile == ""):
		print('concatenation is only possible if the outputfile is specified')
		sys.exit(2)

	print('Command to be executed (Intersections): ', ' '.join(argv))
	for inputfile in args:
		try:
			find_elements(inputfile, outputfile, modality, tierstypes, format, concat)
			if outputfile != "":
				concat = True # always concat for the next files when more than one file
		except KeyError:
			# exc = sys.exception()
			# print("*** print_tb:")
			# traceback.print_tb(exc.__traceback__, limit=1, file=sys.stdout)
			# print("*** print_exception:")
			# traceback.print_exception(exc, limit=2, file=sys.stdout)
			# print("*** print_exc:")
			traceback.print_exc() # (limit=2, file=sys.stdout)
			print('Probably bad format for input file: ' + inputfile)
			pass
		except KeyboardInterrupt:
			sys.exit(1)
		except Exception as e:
			print('Probably bad input file or general error?')
			print(e)
			traceback.print_exc()
			sys.exit(6)
	sys.exit(0)

if __name__ == "__main__":
   compute_intersections(sys.argv[1:])
