# elan-data-analysis

Calcul de statistiques quantitatives sur des ficheirs ELAN.
Implementation en python, il n'est pas nécessaire d'avoir ELAN pour utiliser les outils.

## elan2csv

Cet outil permet de convertir des fichiers ELAN au format CSV de manière différente de l'export réalisé dans le logiciel ELAN.
L'export des lignes principales (lignes sans parent) se fait comme dans la commande ELAN:
- une colonne pour le nom du tier
- trois colonnes pour les temps de début, fin et durée
- une colonne pour le contenu de l'annotation

La différence est pour le traitement des lignes dépendantes. Ces lignes sont extraites selon leur nom de type (et non de tier) dans une colonne correspondante. Ceci fait que les lignes dépendantes de tiers différents mais ayant le même nom de type sont alignées verticalement dans les mêmes colonnes. Cet export s'apparentte à l'export que l'on pourrait faire d'un fichier au format CHAT/CLAN.

## staselan

Cet outil permet de calculer des correspondances entre tiers selon leur seule coincidence temporelle. A l'inverse de l'outil intégré dans ELAN, on peut calculer des coincidences jusqu'à 4 tiers simultanés. Le software vient avec plusieurs options spécifiquement conçues pour le projet DINLANG, mais qui peuvent être facilement customisées pour d'autres projets.

## Utilisation
Les deux outils peuvent fonctionner en ligne de commande python. Une interface web utilisant php est également disponible.

## Disponibilité
Les interfaces web sont disponibles à l'adresse: http://ct3.ortolang.fr/toolselan/

# Tools for ELAN

All tools have a --help option

## ELAN2CSV

Exporting ELAN files to CSV files. 

### elan2csv-main.py
Export ELAN file to a tabular format using the names of the tier (as their type names) so that in the columns after the main lines, each column correspond to a type name, and not as usually in ELAN, to a tier name.

### extractall.py
???

### gettier.py 
Export one tier to a tabular format

## StatsElan

Exporting to a tabular format combinations of specific tiers according to their timing or content

### intersection-main.py

Finds and exports the intersections between several tiers

To express union between tiers it is possible to use the regular expressions.
### askquery.py



