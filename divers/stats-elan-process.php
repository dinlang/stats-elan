<?php

header("Content-Type: text/csv; charset=UTF-8");
header("Acess-Control-Allow-Origin: *");
header("Acess-Control-Allow-Methods: POST");
header("Acess-Control-Allow-Headers: Acess-Control-Allow-Headers,Content-Type,Acess-Control-Allow-Methods, Authorization");

$fileName  =  $_FILES['stats-elan-file']['name'];
$tempPath  =  $_FILES['stats-elan-file']['tmp_name'];
$fileSize  =  $_FILES['stats-elan-file']['size'];
$type = $_POST['stats-elan-type'];
$curdir = getcwd();

if(empty($fileName))
{
	$errorMSG = "<div><dl><dt>message</dt><dd>please select an EAF file</dd><dt>type</dt><dl>".$type."<dt>status</dt>".false."</dl></div>";	
	echo $errorMSG;
}
else
{
	$upload_path = "../eafs/"; // set upload folder path
	$result_path = "../csvs/";
	
	$fileExt = strtolower(pathinfo($fileName,PATHINFO_EXTENSION)); // get image extension
	$fileNameResult = pathinfo($fileName,PATHINFO_FILENAME).".csv";
	header('Content-Disposition: attachment; filename=' . $fileNameResult);
		
	// valid image extensions
	$valid_extensions = array('eaf');
					
	// allow valid image file formats
	if(in_array($fileExt, $valid_extensions))
	{				
		if(file_exists($upload_path . $fileName)) unlink($upload_path . $fileName);

		//check file not exist our upload folder path
		if(!file_exists($upload_path . $fileName))
		{
			// check file size '5MB'
			if($fileSize < 5000000){
				$res = move_uploaded_file($tempPath, $upload_path . $fileName); // move file from system temporary path to our upload folder path
				if ($res != true) {
					$errorMSG = "<div><dl><dt>message</dt><dd>cannot move uploaded file</dd><dt>tmp</dt><dl>".$tempPath."<dt>file</dt>".$upload_path . $fileName."</dl></div>";	
					echo $errorMSG;
				}
			} else {
				$errorMSG = "<div><dl><dt>message</dt><dd>Sorry, your file is too large, please upload 5 MB size</dd><dt>status</dt>".false."</dl></div>";	
				echo $errorMSG;
			}
		}
	} else {
		$errorMSG = "<div><dl><dt>message</dt><dd>Sorry, only EAF files are allowed</dd><dt>status</dt>".false."</dl></div>";	
		echo $errorMSG;		
	}
}
		
// if no error caused, continue ....
if(!isset($errorMSG))
{
	$params = "vocal"; // default
	// if ($type == "vocal") $params = "vocal ";
	if ($type == "visual") $params = "visual";

	// $cmd = 'sh ../php/stats-elan-cmds.sh '.$upload_path.$fileName.' '.$result_path.$fileNameResult.' '.$params;
	// $res = shell_exec($cmd);

	$cmd = "/opt/homebrew/bin/python3 ../src/intersections.py -t ".$params." ".$upload_path.$fileName." -o ".$result_path.$fileNameResult;
	$res = exec($cmd, $output, $return_var);

	if ($return_var == 0) {
		$data = file_get_contents($result_path.$fileNameResult);
		echo $data;
		// echo json_encode(array("curdir" => $curdir, "command" => $cmd, "data" => $data, "output"=> $output, "returnvar" => $return_var, "res" => $res));
	} else {
		$errorMSG = "<div><dl><dt>curdir</dt><dd>".$curdir."</dd><dt>cmd</dt>".$cmd."</dl><dt>output</dt>".$output."</dl><dt>error return value</dt>".$return_var."</dl></div>";	
		// echo json_encode(array("curdir" => $curdir, "command" => $cmd, "res" => $res));
	}
}

?>