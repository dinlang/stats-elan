python3 src/elanquery/gettier.py ~/brainstorm/DINLANG/AllEAF/*.eaf -y act-vis:INTERLOC -s col -e "--" > results/act-vis-interloc.csv
python3 src/elanquery/gettier.py ~/brainstorm/DINLANG/AllEAF/*.eaf -y lng-vis:INTERLOC -s col -e "--" > results/lng-vis-interloc.csv
python3 src/elanquery/gettier.py ~/brainstorm/DINLANG/AllEAF/*.eaf -y act-aud:INTERLOC -s col -e "--" > results/act-aud-interloc.csv
python3 src/elanquery/gettier.py ~/brainstorm/DINLANG/AllEAF/*.eaf -y lng-aud:INTERLOC -s col -e "--" > results/lng-aud-interloc.csv

python3 src/elanquery/gettier.py ~/brainstorm/DINLANG/AllEAF/*.eaf -y act-vis:INTERLOC -s col -e "--" -o results/all-interloc.csv
python3 src/elanquery/gettier.py ~/brainstorm/DINLANG/AllEAF/*.eaf -y lng-vis:INTERLOC -s col -e "--" -o results/all-interloc.csv -c
python3 src/elanquery/gettier.py ~/brainstorm/DINLANG/AllEAF/*.eaf -y act-aud:INTERLOC -s col -e "--" -o results/all-interloc.csv -c
python3 src/elanquery/gettier.py ~/brainstorm/DINLANG/AllEAF/*.eaf -y lng-aud:INTERLOC -s col -e "--" -o results/all-interloc.csv -c
