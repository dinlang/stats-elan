
class TierContent:
	""" data for an Elan tier """
	annot = [] # triplets of time1, time2, content
	name = "default"
	type = "default-type"
	participant = "default-name"
	def __init__(self, a, n, t, p):
		self.annot = a
		self.name = n
		self.type = t
		self.participant = p

def increment(level, cnt, tiers):
	cnt[level] = cnt[level] + 1
	while cnt[level] >= len(tiers[level].annot):
		# the current level is finished
		cnt[level] = 0
		level = level - 1
		if level < 0:
			return
		cnt[level] = cnt[level] + 1 # increment the level below

def allintersections(tiers, filename):
	# tiers information: tab, participant, tiername, tiertype
	nbtiers = len(tiers)
	cnt = [0] * nbtiers
	result = []
	while cnt[0] < len(tiers[0].annot):
		# go through all levels and increment the cnt
		# nothing to do on the first level
		level = 1
		while level < nbtiers:
			# compare this level and the previous one
			if tiers[level].annot[cnt[level]][1] < tiers[level - 1].annot[cnt[level - 1]][0]:
				# the end of the current level and element is smaller than the begining of the previous level and element
				# goto next element on the same level
				for plevel in range(level+1, nbtiers):
					cnt[plevel] = 0
				increment(level, cnt, tiers)
				break # break the inner level loop
			elif tiers[level].annot[cnt[level]][0] > tiers[level - 1].annot[cnt[level - 1]][1]:
				# the begining of the current level and element is greater than the end of the previous level and element
				# finish the current level and add one to the previous level
				for plevel in range(level, nbtiers):
					cnt[plevel] = 0
				level = level - 1
				increment(level, cnt, tiers)
				break # the inner level loop
			else:
				level = level + 1
		if level == nbtiers:
			# compute an intersection
			inter = []
			debs = []
			ends = []
			for k in range(0, nbtiers):
				inter.append(tiers[k].annot[cnt[k]][0])
				inter.append(tiers[k].annot[cnt[k]][1])
				inter.append(tiers[k].annot[cnt[k]][1] - tiers[k].annot[cnt[k]][0])
				inter.append(tiers[k].annot[cnt[k]][2])
				inter.append(tiers[k].participant)
				inter.append(tiers[k].name)
				inter.append(tiers[k].type)
				debs.append(tiers[k].annot[cnt[k]][0])
				ends.append(tiers[k].annot[cnt[k]][1])
			# if the intersection is valid, add it
			begininter = max(debs)
			endinter = min(ends)
			durinter = endinter - begininter
			if (begininter < endinter):
				inter.append(begininter)
				inter.append(endinter)
				inter.append(durinter)
				inter.append(filename)
				result.append(inter)
			increment(level-1, cnt, tiers)
	return result
