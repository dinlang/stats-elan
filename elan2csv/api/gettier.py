import pympi
import os

def printcoltier(fn, participant, listtier, exclude):
	for c1 in listtier:
		if (c1[2] == exclude): continue
		print(fn + '\t' + participant + '\t' + c1[2] + '\t' + str(c1[0]) + '\t' + str(c1[1]))

# parameters:
#	eaf_file = descripteur ELAN
#   participant = nom du participant
#   first = liste d'annotation pour un tier
#   others = liste de tiers à décrire dans le temps des annotations de first
def printother(fn, eaf_file, participant, first, others, exclude):
	(eaf_start, eaf_end) = eaf_file.get_full_time_interval()
	for c1 in first:
		if (c1[2] == exclude): continue
		line = c1[2]
		notfound = False
		for c2 in range(1, len(others)):
			t12 = eaf_file.get_annotation_data_between_times(others[c2], c1[0]+1, c1[1]-1)
#			print(c2, others[c2], t12)
			if (len(t12) > 0):
				if (t12[0][2] == exclude):
					notfound = True
					break
				line = line + '\t' + t12[0][2]
			else:
				notfound = True
				break
		if (notfound == False):
			d = c1[1] - c1[0]
			print(fn + '\t' + participant + '\t' + line + '\t' + str(c1[0]) + '\t' + str(c1[1]) + '\t' + str(d) + '\t' + str(d/(eaf_end-eaf_start)))

def printother2(fn, eaf_file, participant, first, others, exclude):
	(eaf_start, eaf_end) = eaf_file.get_full_time_interval()
	for c1 in first:
		if (c1[2] == exclude): continue
		infoc0 = eaf_file.get_parameters_for_tier(others[0])
		line = infoc0['LINGUISTIC_TYPE_REF'] + '\t' + others[0] + '\t' + c1[2]
		notfound = False
		for c2 in range(1, len(others)):
			t12 = eaf_file.get_annotation_data_between_times(others[c2], c1[0]+1, c1[1]-1)
#			print(c2, others[c2], t12)
			if (len(t12) > 0):
				if (t12[0][2] == exclude):
					notfound = True
					break
				infoc2 = eaf_file.get_parameters_for_tier(others[c2])
				line = line + '\t' + infoc2['LINGUISTIC_TYPE_REF'] + '\t' + others[c2] + '\t' + t12[0][2]
			else:
				notfound = True
				break
		if (notfound == False):
			d = c1[1] - c1[0]
			print(fn + '\t' + participant + '\t' + line + '\t' + str(c1[0]) + '\t' + str(c1[1]) + '\t' + str(d) + '\t' + str(d/(eaf_end-eaf_start)))

def printgettier(fn, eaf_file, tier, style, exclude):
	tier_names = eaf_file.get_tier_names()
	tiers = tier.split(':')
	if (len(tiers) > 1):
		if (tiers[0] not in tier_names):
			print('Error: cannot find tier name: ' + tiers[0])
			sys.exit(2)
		first_tier = eaf_file.get_annotation_data_for_tier(tiers[0])
		param_tier = eaf_file.get_parameters_for_tier(tiers[0])
		printother(fn, eaf_file, param_tier['PARTICIPANT'], first_tier, tier, exclude)
	elif (tier != ''):
		if (tier not in tier_names):
			print('Error: cannot find tier name: ' + tier)
			sys.exit(2)
		param_tier = eaf_file.get_parameters_for_tier(tier)
		list_tier = eaf_file.get_annotation_data_for_tier(tier)
		if (style.startswith('params') or style.startswith('all')):
			print('Filename: ')
			print(fn)
			print('Tier params:')
			print(param_tier)
		if (style.endswith('row') or style.startswith('all')):
			print('Filename: ')
			print(fn)
			print('Tier contents:')
			print(param_tier['PARTICIPANT'])
			print(list_tier)
		elif (style.endswith('col')):
			printcoltier(fn, param_tier['PARTICIPANT'], list_tier, exclude)

def printeafinfo(fn, eaf_file):
	tier_names = eaf_file.get_tier_names()
	type_names = eaf_file.get_linguistic_type_names()
	print('Filename: ')
	print(fn)
	print('List of tiers: ')
	print(tier_names)
	print('List of types: ')
	print(type_names)

def printtypeotherfinal(fn, eaf_file, participant, first_tier, calltiers, exclude):
	# print("A faire:")
	# print(participant)
	# print(first_tier)
	# print(calltiers)
	# print("Résultat:")
	annot_first_tier = eaf_file.get_annotation_data_for_tier(first_tier)
	printother2(fn, eaf_file, participant, annot_first_tier, calltiers, exclude)

def	printtypeotherinside(fn, eaf_file, participant, first_tier, types, ty, calltiers, exclude):
	if ty >= len(types):
		printtypeotherfinal(fn, eaf_file, participant, first_tier, calltiers, exclude)
		return
	# trouver les tiers de type types[ty] dont le parent est first_tier
	# print("inside: " + str(ty) + " " + types[ty] + " " + first_tier)
	nexttiers = eaf_file.get_tier_ids_for_linguistic_type(types[ty], first_tier)
	for insidetier in nexttiers:
		calltiers[ty] = insidetier
		printtypeotherinside(fn, eaf_file, participant, first_tier, types, ty+1, calltiers, exclude)

def printtypeother(fn, eaf_file, first_type, types, exclude):
	# print(types)
	first_types = eaf_file.get_tier_ids_for_linguistic_type(first_type)
	for first_tier in first_types:
		calltiers = [''] * len(types)
		calltiers[0] = first_tier
		first_tier_params = eaf_file.get_parameters_for_tier(first_tier)
		# print(first_tier)
		participant = first_tier_params['PARTICIPANT']
		# print(participant)
		printtypeotherinside(fn, eaf_file, participant, first_tier, types, 1, calltiers, exclude)

#			for nt in nexttiers:

def printgettype(fn, eaf_file, type, style, exclude):
	type_names = eaf_file.get_linguistic_type_names()
	types = type.split(':')
	if (len(types) > 1):
		if (types[0] not in type_names):
			print('Error: cannot find type name: ' + types[0])
			sys.exit(2)
		printtypeother(fn, eaf_file, types[0], types, exclude)
	elif (type != ''):
		param_type = eaf_file.get_parameters_for_linguistic_type(type)
		if (style.startswith('params') or style.startswith('all')):
			print('Filename: ')
			print(fn)
			print('Type params:')
			print(param_type)
			print('Type tiers:')
			tiers_type = eaf_file.get_tier_ids_for_linguistic_type(type)
			print(tiers_type)
		if (style.startswith('col') or style.startswith('raw') or style.startswith('all')):
			listoftiers = eaf_file.get_tier_ids_for_linguistic_type(type)
			for tier in listoftiers:
				printgettier(fn, eaf_file, tier, style, exclude)

def gettier(inputfile, tier, type, participant, style, exclude):
	# test if input file exists
	if (not os.path.isfile(inputfile)):
		print("the inputfile does not exist")
		sys.exit(5)
	# open the .eaf file
	eaf_file = pympi.Elan.Eaf(inputfile)

	if (style.startswith('list') or style.startswith('all')):
		printeafinfo(inputfile, eaf_file)

	printgettier(inputfile, eaf_file, tier, style, exclude)
	printgettype(inputfile, eaf_file, type, style, exclude)

# [optionnel] Arrondi des temps de début/fin à l'image près
# df['AnnotationBeginTime'] = (df['AnnotationBeginTime'] / 40).apply(np.floor) * 40 / 1000
# df['AnnotationEndTime'] = (df['AnnotationEndTime'] / 40).apply(np.floor) * 40 / 1000

import sys, getopt

def printhelp():
	print('gettier.py <inputfiles ...> [-t <tier> -y <type> -p <participant> -s <style> -o <outfile>  -e <exclude> ]')
	print('   inputfiles = listes des fichiers à traiter')
	print('   -o outfile = fichier résultat au format csv')
	print('   -t tier = nom du(des) tier(s) à chercher')
	print('   -y type = nom du type à chercher')
	print('   -p participant = nom du participant à chercher')
	print('   -s style = type d\'affichage: all list type params /raw /col')
	print('   -e motif = to exclude from print')
	print('   -c = concat')
	print('   -h = this help')

def printnotice():
	print('gettier.py <inputfiles ...> [-t <tier> -y <type> -p <participant> -s <style>]')
	print('gettier.py -h ; for help')

def main(argv):
	participant = 'all'
	type = ''
	tier = ''
	style = 'all/raw'
	outputfile = ''
	concat = False
	exclude = ''
	try:
		opts, args = getopt.gnu_getopt(argv,"hct:y:p:s:o:e:", ["help", "concat", "type=", "tier=", "part=", "style=", "output=", "exclude="])
	except getopt.GetoptError:
		printnotice()
		sys.exit(3)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			printhelp()
			sys.exit(2)
		elif opt in ("-t", "--type"):
			tier = arg
		elif opt in ("-p", "--part"):
			participant = arg
		elif opt in ("-y", "--type"):
			type = arg
		elif opt in ("-c", "--concat"):
			concat = True
		elif opt in ("-o", "--output"):
			outputfile = arg
		elif opt in ("-e", "--exclude"):
			exclude = arg
		elif opt in ("-s", "--style"):
			style = arg
	if (outputfile != ''):
		original_stdout = sys.stdout
		ope = 'a' if concat == True else 'w'
		fn = open(outputfile, ope)
		sys.stdout = fn # Change the standard output to the file we created.
	for inputfile in args:
		try:
			gettier(inputfile, tier, type, participant, style, exclude)
		except KeyError:
			print('Probably bad format for input file: ' + inputfile)
			pass
		except KeyboardInterrupt:
			sys.exit(1)
	if (outputfile != ''):
		sys.stdout = original_stdout # Reset the standard output to its original value

if __name__ == "__main__":
   main(sys.argv[1:])
