import pympi
import pandas as pd
import numpy as np
import os

# get_ref_annotation_data_for_tier
# lng_vis = eaf_file.get_annotation_data_for_tier('lng-vis-' + participant)

def process_file(inputfile, outputfile):
	# Ouverture d'un fichier .eaf
	eaf_file = pympi.Elan.Eaf(inputfile)
	# types_names = eaf_file.get_linguistic_type_names()
	# for tn in types_names:
	# 	params = eaf_file.get_parameters_for_linguistic_type(tn)
	# 	print(tn)
	# 	print(params)
	# 	print(params['TIME_ALIGNABLE'])

	# tier_names = eaf_file.get_tier_names()
	# for tn in tier_names:
	# 	params = eaf_file.get_parameters_for_tier(tn)
	# 	print(tn)
	# 	print(params)
	# 	annot = eaf_file.get_annotation_data_for_tier(tn)
	# 	print(annot)

		# if "@AUD" in tn:
		# 	# Récupération des annotations des pistes
		# 	annot = eaf_file.get_annotation_data_for_tier(tn)
		# 	print(tn)
		# on récupère des triplets temps début, temps fin, annotation
		#nlvis = np.argsort([x[0] for x in lng_vis])

	# fill big table
	tier_names = eaf_file.get_tier_names()
	table_dict = {}
	table_row_names = {}
	for tn in tier_names:
		annots = eaf_file.get_annotation_data_for_tier(tn)
		for a in annots:
			# print(a)
			# a[0] time1, a[1] time2, a[3] value
			table_row_name = str(a[0]) + "-" + str(a[1])
			if table_row_name not in table_row_names:
				table_row_names[table_row_name] = (a[0], a[1])
				table_dict[table_row_name] = {}
			table_dict[table_row_name][tn] = a[2]

	# create a table with the table_dict
	table = []
	for r in table_row_names:
		row = []
		row.append(str(table_row_names[r][0]))
		row.append(str(table_row_names[r][1]))
		for c in tier_names:
			if c in table_dict[r]:
				row.append(table_dict[r][c])
			else:
				row.append('')
		table.append(row)

	head_names = list(tier_names)
	head_names.insert(0, 'end')
	head_names.insert(0, 'start')

	df = pd.DataFrame(table, columns=head_names)
	# Sauvegarde de la dataframe au format .csv
	# if file does not exist write header 
	if not os.path.isfile(outputfile):
		df.to_csv(outputfile, header='column_names', index=False)
	else: # else it exists so append without writing the header
		df.to_csv(outputfile, mode='a', header=False, index=False)

# [optionnel] Arrondi des temps de début/fin à l'image près
# df['AnnotationBeginTime'] = (df['AnnotationBeginTime'] / 40).apply(np.floor) * 40 / 1000
# df['AnnotationEndTime'] = (df['AnnotationEndTime'] / 40).apply(np.floor) * 40 / 1000

import sys, getopt

def main(argv):
	outputfile = ''
	try:
		opts, args = getopt.gnu_getopt(argv,"ho:",["help:ofile="])
	except getopt.GetoptError:
		print('extractall.py <inputfile ...> -o <outputfile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('extractall.py <inputfile ...> -o <outputfile>')
			print('   inputfile = listes des fichiers à traiter')
			print('   -o outfile = fichier résultat au format csv')
			sys.exit()
		elif opt in ("-o", "--ofile"):
			outputfile = arg
	if len(args) < 1:
		print('Erreur: pas de fichier à traiter')
		print('extractall.py <inputfile ...> -o <outputfile>')
		sys.exit(2)
	if outputfile == '':
		print('Erreur: pas de nom de fichier résultat')
		print('extractall.py <inputfile ...> -o <outputfile>')
		sys.exit(2)
	for inputfile in args:
		process_file(inputfile, outputfile)

if __name__ == "__main__":
   main(sys.argv[1:])
