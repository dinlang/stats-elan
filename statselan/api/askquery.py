# askquery.py
import sys
import getopt
import traceback

# sys.path.insert(1, "/applis/toolselan/bibl/python/")
# sys.path.insert(1, "/applis/dev/toolselan/bibl/python/")
sys.path.insert(1, "/Users/cp/devlopt/elan-data-analysis/bibl/python/")

import utils as utils
import pandas as pd


def init_askquery_classic(filename, fusion=0):
    bloc1_columntypes = {
        'Annotation1': str,
        'AnnotationBeginTime1': int,
        'AnnotationEndTime1': int,
        'AnnotationDuration1': int,
        'TierName1': str,
        'TierType1': str,
        'TierParticipant1': str,
    }

    if fusion == 12:
        bloc2_columntypes = {
            'Annotation2': str,
            'AnnotationBeginTime2': int,
            'AnnotationEndTime2': int,
            'AnnotationDuration2': int,
            'TierName2': str,
            'TierType2': str,
            'Fusion12': str,
        }
    else:
        bloc2_columntypes = {
            'Annotation2': str,
            'AnnotationBeginTime2': int,
            'AnnotationEndTime2': int,
            'AnnotationDuration2': int,
            'TierName2': str,
            'TierType2': str,
            'TierParticipant2': str,
        }

    bloc3_columntypes = {
        'Annotation3': str,
        'AnnotationBeginTime3': int,
        'AnnotationEndTime3': int,
        'AnnotationDuration3': int,
        'TierName3': str,
        'TierType3': str,
        'TierParticipant3': str,
    }

    if fusion == 34:
        bloc4_columntypes = {
            'Annotation4': str,
            'AnnotationBeginTime4': int,
            'AnnotationEndTime4': int,
            'AnnotationDuration4': int,
            'TierName4': str,
            'TierType4': str,
            'Fusion34': str,
        }
    else:
        bloc4_columntypes = {
            'Annotation4': str,
            'AnnotationBeginTime4': int,
            'AnnotationEndTime4': int,
            'AnnotationDuration4': int,
            'TierName4': str,
            'TierType4': str,
            'TierParticipant4': str,
        }

    blocend_columntypes = {
        'IntersectionBegin': int,
        'IntersectionEnd': int,
        'IntersectionDuration': int,
        'TranscriptionName': str
    }

    blocend12_columntypes = {
        'IntersectionBegin12': int,
        'IntersectionEnd12': int,
        'IntersectionDuration12': int,
        'TranscriptionName12': str
    }

    blocend34_columntypes = {
        'IntersectionBegin34': int,
        'IntersectionEnd34': int,
        'IntersectionDuration34': int,
        'TranscriptionName': str
    }

    blocend3_columntypes = {
        'IntersectionBegin': int,
        'IntersectionEnd': int,
        'IntersectionDuration': int
    }

    data_frame = pd.read_csv(filename)
    # print(data_frame.head())
    print("Nb rows: ", data_frame.shape[0], " Nb cols: ", data_frame.shape[1])
    width = data_frame.shape[1]
    if width == 11:
        new_df = bloc1_columntypes
        new_df.update(blocend_columntypes)
    elif width == 18:
        new_df = bloc1_columntypes
        new_df.update(bloc2_columntypes)
        new_df.update(blocend_columntypes)
    elif width == 25:
        new_df = bloc1_columntypes
        new_df.update(bloc2_columntypes)
        new_df.update(bloc3_columntypes)
        new_df.update(blocend_columntypes)
    elif width == 32:
        new_df = bloc1_columntypes
        new_df.update(bloc2_columntypes)
        new_df.update(bloc3_columntypes)
        new_df.update(bloc4_columntypes)
        new_df.update(blocend_columntypes)
    elif width == 39:
        new_df = bloc1_columntypes
        new_df.update(bloc2_columntypes)
        new_df.update(blocend12_columntypes)
        new_df.update(bloc3_columntypes)
        new_df.update(bloc4_columntypes)
        new_df.update(blocend34_columntypes)
        new_df.update(blocend3_columntypes)
        # elif width == 60:
        #     new_df = bloc1_columntypes
        #     new_df.update(bloc2_columntypes)
        #     new_df.update(blocend12_columntypes)
        #     new_df.update(bloc3_columntypes)
        #     new_df.update(bloc4_columntypes)
        #     new_df.update(blocend34_columntypes)
        #     new_df.update(blocend3_columntypes)
        #     new_df.update(bloc4_columntypes)
        #     new_df.update(bloc4_columntypes)
        #     new_df.update(bloc4_columntypes)
    else:
        print("Unimplemented number of columns: " + str(
            width) + '. Cannot do query. Did you have the correct intersection query?')
        exit(1)

    return (data_frame, new_df)


def init_askquery_advanced(filename, fusion=0):
    # blocend3_columntypes = {
    #     'IntersectionBegin': int,
    #     'IntersectionEnd': int,
    #     'IntersectionDuration': int
    # }

    data_frame = pd.read_csv(filename)
    print(data_frame.head())
    print("Nb rows: ", data_frame.shape[0], " Nb cols: ", data_frame.shape[1])
    cls = list(data_frame.columns)
    if fusion == 12:
        # data_frame.rename(columns={'TierParticipant2', 'Fusion12'}, inplace=True)
        if 'TierParticipant2' not in cls:
            print('No TierParticipant2: cannot do fusion12')
            sys.exit(1)
        idxf = cls.index('TierParticipant2')
        cls[idxf] = 'Fusion12'
    elif fusion == 34:
        # data_frame.rename(columns={'TierParticipant4', 'Fusion34'}, inplace=True)
        if 'TierParticipant4' not in cls:
            print('No TierParticipant4: cannot do fusion34')
            sys.exit(1)
        idxf = cls.index('TierParticipant4')
        cls[idxf] = 'Fusion34'
    # ndf = data_frame.columns
    # print(ndf)
    # print(type(ndf))
    print(cls)
    if 'IntersectionDuration' not in cls:
        print('adding three columns')
        # duplication of 'AnnotationBeginTime1', 'AnnotationEndTime1', 'AnnotationDuration1'
        # to create 'IntersectionBegin', 'IntersectionEnd', 'IntersectionDuration'
        data_frame['IntersectionBegin'] = data_frame.loc[:, 'AnnotationBeginTime1']
        data_frame['IntersectionEnd'] = data_frame.loc[:, 'AnnotationEndTime1']
        data_frame['IntersectionDuration'] = data_frame.loc[:, 'AnnotationDuration1']
        cls = list(data_frame.columns)
    return data_frame, cls


def comparaison_participant_value(part, value):
    if value == "1-M" and part == "mère": return True
    if value == "1-P" and part == "père": return True
    if value == "1-Ea" and part == "aîné": return True
    if value == "1-Eb" and part == "deuxième": return True
    if value == "1-Ec" and part == "troisième": return True
    return False


# 2a. … quand X LNG (aud et/ou vis) à Y, est-ce que Y regarde X ?
def value2_is_participant1(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        # if comparaison_participant_value(old_row['TierParticipant1'], old_row['Annotation2']) == true:
        if ((old_row['Annotation2'] == "1-M" and old_row['TierParticipant1'] == "mère") or
                (old_row['Annotation2'] == "1-P" and old_row['TierParticipant1'] == "père") or
                (old_row['Annotation2'] == "1-Ea" and old_row['TierParticipant1'] == "aîné") or
                (old_row['Annotation2'] == "1-Eb" and old_row['TierParticipant1'] == "deuxième") or
                (old_row['Annotation2'] == "1-Ec" and old_row['TierParticipant1'] == "troisième")
        ):
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("Annotation2 is matched with participant1")
    print(ndf)
    return new_df_val


# 2b. … quand X LNG (aud et/ou vis) à Y, est-ce que X regarde Y ET Y regarde X ?
def value3_is_participant1and2_and_reverse(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        if (
                (old_row['TierParticipant1'] == old_row['TierParticipant2']) and
                (comparaison_participant_value(old_row['TierParticipant2'], old_row['Annotation3']) is True) and
                (old_row['Annotation1'] == old_row['Annotation2']) and
                (comparaison_participant_value(old_row['TierParticipant3'], old_row['Annotation2']) is True)
        ):
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("Annotation1 and value2 are matched with participant3 and participant1 and 2 are matched with value3")
    print(ndf)
    return new_df_val


def value3_is_participant1or2(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        if ((old_row['Annotation3'] == "1-M" and (
                old_row['TierParticipant1'] == "mère" or old_row['TierParticipant2'] == "mère")) or
                (old_row['Annotation3'] == "1-P" and (
                        old_row['TierParticipant1'] == "père" or old_row['TierParticipant2'] == "père")) or
                (old_row['Annotation3'] == "1-Ea" and (
                        old_row['TierParticipant1'] == "aîné" or old_row['TierParticipant2'] == "aîné")) or
                (old_row['Annotation3'] == "1-Eb" and (
                        old_row['TierParticipant1'] == "deuxième" or old_row['TierParticipant2'] == "deuxième")) or
                (old_row['Annotation3'] == "1-Ec" and (
                        old_row['TierParticipant1'] == "troisième" or old_row['TierParticipant2'] == "troisième"))
        ):
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("Annotation1 is matched with participant2")
    print(ndf)
    return new_df_val


def participant1_is_participant2(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        if old_row['TierParticipant1'] == old_row['TierParticipant2']:
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("participant1 is matched with participant2")
    print(ndf)
    return new_df_val


def participant1_is_participant3(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        if old_row['TierParticipant1'] == old_row['TierParticipant3']:
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("participant1 is matched with participant3")
    print(ndf)
    return new_df_val


def replace_fusion12(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = dict(odf.iloc[index])
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        if old_row['TierParticipant1'] == old_row['TierParticipant2']:
            if old_row['Annotation1'] == old_row['Annotation2']:
                old_row['Fusion12'] = old_row['Annotation1']
            elif old_row['Annotation1'] == "--":
                old_row['Fusion12'] = old_row['Annotation2']
            elif old_row['Annotation2'] == "--":
                old_row['Fusion12'] = old_row['Annotation1']
            else:
                old_row['Fusion12'] = 'different'
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("participant1 is matched with participant2")
    print(ndf)
    return new_df_val


def replace_fusion34(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = dict(odf.iloc[index])
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        if old_row['TierParticipant3'] == old_row['TierParticipant4']:
            if old_row['Annotation3'] == old_row['Annotation4']:
                old_row['TierParticipant4'] = old_row['Annotation3']
            elif old_row['Annotation3'] == "--":
                old_row['TierParticipant4'] = old_row['Annotation4']
            elif old_row['Annotation4'] == "--":
                old_row['TierParticipant4'] = old_row['Annotation3']
            else:
                old_row['TierParticipant4'] = 'different'
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("participant3 is matched with participant4")
    print(ndf)
    return new_df_val


def participant1or2_is_participant3(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        if (old_row['TierParticipant1'] == old_row['TierParticipant3']) or (
                old_row['TierParticipant2'] == old_row['TierParticipant3']):
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("participant1 or 2 is matched with participant3")
    print(ndf)
    return new_df_val


# 3a. … quand X LNG (aud et/ou vis) à Y (Y=non-Z) ET Z ne LNG pas, est-ce que Z regarde X ?
# à mettre en regard avec total de X parle à Y (Y=non-Z)
# LNG part1 = X value1 = Y LNG part2 = Z value2 "--" ?? REG part3 = Z value3 = X ??
def langage_non_adresse_enfant_regarde_speaker_123(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        if (
                (old_row['TierParticipant1'] != '--')
                and (old_row['Annotation1'] != '--')
                and (old_row['Annotation1'] != '0-self')
                and (old_row['TierParticipant2'] != old_row['TierParticipant1'])
                and (old_row['TierParticipant2'] != '--')
                and (old_row['Annotation2'] == '--')
                and (comparaison_participant_value(old_row['TierParticipant2'], old_row['Annotation1']) is False)
                and (old_row['TierParticipant2'] == old_row['TierParticipant3'])
                and (old_row['Annotation3'] != '0-self')
        ):
            if comparaison_participant_value(old_row['TierParticipant1'], old_row['Annotation3']) is True:
                # old_row_list = list(old_row)
                # old_row_list['TierParticipant3'] = 'regarde_speaker_qui_ne_lui_parle_pas'
                # old_row = tuple(old_row_list)
                old_row['TierParticipant3'] = 'regarde_speaker_qui_ne_lui_parle_pas'
            else:
                # old_row_list = list(old_row)
                # old_row_list['TierParticipant3'] = 'neregardepas_speaker_qui_ne_lui_parle_pas'
                # old_row = tuple(old_row_list)
                old_row['TierParticipant3'] = 'neregardepas_speaker_qui_ne_lui_parle_pas'
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("lnae regarde speaker")
    print(ndf)
    return new_df_val


# 4a. … quand X LNG (aud et/ou vis) à Y (Y=non-Z) ET Z ne LNG pas, est-ce que Z regarde Y ?
#  - à mettre en regard avec total de X parle à Z
# LNG part1 = X value1 = Y LNG part2 = Z value2 "--" ?? REG part3 = Z value3 = Y ??
def langage_non_adresse_enfant_regarde_adressee_123(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        if (
                (old_row['TierParticipant1'] != '--')
                and (old_row['Annotation1'] != '--')
                and (old_row['Annotation1'] != '0-self')
                and (old_row['TierParticipant2'] != old_row['TierParticipant1'])
                and (old_row['TierParticipant2'] != '--')
                and (old_row['Annotation2'] == '--')
                and (comparaison_participant_value(old_row['TierParticipant2'], old_row['Annotation1']) is False)
                and (old_row['TierParticipant2'] == old_row['TierParticipant3'])
                and (old_row['Annotation3'] != '0-self')
        ):
            if old_row['Annotation3'] == old_row['Annotation1']:
                # old_row_list = list(old_row)
                # old_row_list['TierParticipant3'] = 'regarde_adressee'
                # old_row = tuple(old_row_list)
                old_row['TierParticipant3'] = 'regarde_adressee'
            else:
                # old_row_list = list(old_row)
                # old_row_list['TierParticipant3'] = 'neregardepas_adressee'
                # old_row = tuple(old_row_list)
                old_row['TierParticipant3'] = 'neregardepas_adressee'
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("lnae regarde adressee")
    print(ndf)
    return new_df_val


# x_parle_a_y_et_x_regarde_y
def qui_est(interloc):
    if '-M' in interloc:
        return "mère"
    if '-P' in interloc:
        return "père"
    if '-Ea' in interloc:
        return "aîné"
    if '-Eb' in interloc:
        return "deuxième"
    if '-Ec' in interloc:
        return "troisième"
    return interloc


def x_parle_a_y_et_x_regarde_y(odf, ndf):
    # x (lng) TierParticipant1_1 père, mère, aîné, deuxième, troisième
    # y (lng) Fusion12_1 -M -P -Ea -Eb -Ec everybody different (voir Annotation2_1)
    # x (reg) TierParticipant1_2
    # y (reg) Annotation2_2
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        new_row = old_row.to_list()
        if old_row['Fusion12_1'] == '--':
            result = 'nona'
        elif old_row['Annotation1_2'] != 'participant':
            result = 'nona'
        else:
            x_lng = old_row['TierParticipant1_1']
            y_lng = old_row['Fusion12_1']
            x_reg = old_row['TierParticipant1_2']
            y_reg = old_row['Annotation2_2']
            if x_lng != x_reg:
                result = 'nona'
            else:
                yl = qui_est(y_lng)
                yr = qui_est(y_reg)
                if yl == yr or yl == 'everybody' or yr == 'everybody':
                    result = 'true'
                else:
                    result = 'false'
            new_row.append(result)
            newdata.append(new_row)
    ndf.append("x_parle_a_y_et_x_regarde_y")
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("x_parle_a_y_et_x_regarde_y")
    print(new_df_val)
    return new_df_val


def y_parle_a_x_et_x_regarde_y(odf, ndf):
    # x (lng) Fusion12_1 -M -P -Ea -Eb -Ec everybody different (voir Annotation2_1)
    # y (lng) TierParticipant1_1 père, mère, aîné, deuxième, troisième
    # x (reg) TierParticipant1_2
    # y (reg) Annotation2_2
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        new_row = old_row.to_list()
        if old_row['Fusion12_1'] == '--':
            result = 'nona'
        elif old_row['Annotation1_2'] != 'participant':
            result = 'nona'
        else:
            x_lng = old_row['Fusion12_1']
            y_lng = old_row['TierParticipant1_1']
            x_reg = old_row['TierParticipant1_2']
            y_reg = old_row['Annotation2_2']
            xl = qui_est(x_lng)
            yr = qui_est(y_reg)
            if xl != x_reg and xl != 'everybody':
                result = 'nona'
            else:
                if y_lng == yr or yr == 'everybody':
                    result = 'true'
                else:
                    result = 'false'
            new_row.append(result)
            newdata.append(new_row)
    ndf.append("y_parle_a_x_et_x_regarde_y")
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("y_parle_a_x_et_x_regarde_y")
    print(new_df_val)
    return new_df_val


# 3b. … quand X LNG (aud et/ou vis) à Z ET Z ne LNG pas, est-ce que Z regarde X ?
# à mettre en regard avec total de X parle à Z
# LNG part1 = X value1 = Z LNG part2 = Z value2 "--" ?? REG part3 = Z value3 = X ??
def langage_sans_reponse_enfant_123(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        if (
                (old_row['TierParticipant1'] != '--')
                and (old_row['Annotation1'] != '--')
                and (old_row['Annotation1'] != '0-self')
                and (old_row['TierParticipant2'] != old_row['TierParticipant1'])
                and (old_row['TierParticipant2'] != '--')
                and (old_row['Annotation2'] == '--')
                and (comparaison_participant_value(old_row['TierParticipant2'], old_row['Annotation1']) is True)
                and (old_row['TierParticipant2'] == old_row['TierParticipant3'])
                and (old_row['Annotation3'] != '0-self')
        ):
            if comparaison_participant_value(old_row['TierParticipant1'], old_row['Annotation3']) is True:
                # old_row_list = list(old_row)
                # old_row_list['TierParticipant3'] = 'regarde_speaker_qui_lui_parle'
                # old_row = tuple(old_row_list)
                old_row['TierParticipant3'] = 'regarde_speaker_qui_lui_parle'
            else:
                # old_row_list = list(old_row)
                # old_row_list['TierParticipant3'] = 'neregardepas_speaker_qui_lui_parle'
                # old_row = tuple(old_row_list)
                old_row['TierParticipant3'] = 'neregardepas_speaker_qui_lui_parle'
            newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("participant1 is matched with participant2")
    print(ndf)
    return new_df_val


def same_participant(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        part = old_row['TierParticipant1']
        if 'TierParticipant2' in old_row and old_row['TierParticipant2'] != part:
            continue
        if 'TierParticipant3' in old_row and old_row['TierParticipant3'] != part:
            continue
        if 'TierParticipant4' in old_row and old_row['TierParticipant4'] != part:
            continue
        if 'TierParticipant5' in old_row and old_row['TierParticipant5'] != part:
            continue
        newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("all the participants are the same")
    print(ndf)
    return new_df_val


def notiret(odf, ndf):
    nbrow = len(odf)
    newdata = list()
    for index in range(0, nbrow):
        old_row = odf.iloc[index]
        #    print(old_row['Annotation1'] + ' ' + old_row['TierParticipant2'])
        if old_row['Annotation1'] == '--':
            continue
        if 'Annotation2' in old_row and old_row['Annotation2'] == '--':
            continue
        if 'Annotation3' in old_row and old_row['Annotation3'] == '--':
            continue
        if 'Annotation4' in old_row and old_row['Annotation4'] == '--':
            continue
        if 'Annotation5' in old_row and old_row['Annotation5'] == '--':
            continue
        newdata.append(old_row)
    new_df_val = pd.DataFrame(newdata, columns=ndf)
    print("remove all '--'")
    print(ndf)
    return new_df_val


def load_and_intersect(inputs, options=''):
    if len(inputs) != 2:
        print("Intersect is done between 2 files exactly.")
        print("Input files were: ", inputs)
        return
    (odf1, ndf1) = init_askquery_advanced(inputs[0])
    (odf2, ndf2) = init_askquery_advanced(inputs[1])
    # odf1.sort_values(['IntersectionBegin', 'IntersectionEnd'], ascending=[True, True])
    # odf2.sort_values(['IntersectionEnd', 'IntersectionBegin'], ascending=[True, True])
    # AnnotationBeginTime4
    nbrow1 = len(odf1)
    nbrow2 = len(odf2)
    new_data = list()
    # name1 = odf1.iloc[0]['TranscriptionName']
    # name2 = odf2.iloc[0]['TranscriptionName']
    # if name1 != name2:
    #     print('Intersection should be done with files issued from the same EAF file')
    #     print('Filenames are: ', name1, name2)
    #     sys.exit(1)
    for index1 in range(0, nbrow1):
        if index1 % 100 == 0:
            print(index1)
        old_row1 = odf1.iloc[index1]
        tstart1 = old_row1['IntersectionBegin']
        tstop1 = old_row1['IntersectionEnd']
        fname1 = old_row1['TranscriptionName']
        for index2 in range(0, nbrow2):
            old_row2 = odf2.iloc[index2]
            tstart2 = old_row2['IntersectionBegin']
            tstop2 = old_row2['IntersectionEnd']
            fname2 = old_row2['TranscriptionName']
            if fname1 != fname2:
                continue
            inter_start = max(tstart1, tstart2)
            inter_stop = min(tstop1, tstop2)
            if inter_start < inter_stop:
                #                print("OK", index1, index2, tstart1, tstop1, tstart2, tstop2)
                new_row = old_row1.to_list() + old_row2.to_list()
                duration = inter_stop - inter_start
                new_row.append(inter_start)
                new_row.append(inter_stop)
                new_row.append(duration)
                new_row.append(fname2)
                new_data.append(new_row)
                # print(type(new_row))
                # print(new_row)
            # else:
            #     print("NO", index1, index2, tstart1, tstop1, tstart2, tstop2)
            # if tstart1 > tstop2:
            #     break
    #    new_key = ndf1 + ndf2
    ncol1 = [s + "_1" for s in ndf1]
    ncol2 = [s + "_2" for s in ndf2]
    ncol1 = ncol1 + ncol2
    ncol1 = ncol1 + ['IntersectionBegin', 'IntersectionEnd', 'IntersectionDuration', 'TranscriptionName']
    new_df_val = pd.DataFrame(new_data, columns=ncol1)
    print("intersection")
    return new_df_val


action_list = ["annot2ispart1", "annot3ispart1or2", "annot3ispart1and2andreverse", "part1ispart2", "part1ispart3",
               "part1or2ispart3", "notiret", "x_parle_a_y_et_x_regarde_y", "y_parle_a_x_et_x_regarde_y",
               "lnaera123", "lnaers123", "lsre123",
               "samepart", "fusion12", "fusion34"]


def printhelp():
    print('askquery.py <inputfiles ...> -o <outputfile> -a <action> -f <format> --intersect')
    print('   inputfiles = listes des fichiers a traiter')
    print('   -o outfile = fichier resultat au format csv')
    print('   --intersect intersection of two files')
    print('   -a action = filter action to be executed in: ' + ' '.join(action_list))
    print('   -f format = file output format: CSV or XLSX')


def printnotice():
    print('askquery.py <inputfiles ...> -o <outputfile> -a <action> -f <format>')
    print('askquery.py -h ; for help')


def askquery(argv):
    outputfile = ''
    action = ''
    format = ""
    intersect = False
    intersectoptions = ''
    try:
        opts, args = getopt.gnu_getopt(argv, "a:o:f:h",
                                       ["action=", "ofile=", "format=", "help", "intersect"])  # , "brut"])
    except getopt.GetoptError:
        printnotice()
        sys.exit(3)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            printhelp()
            sys.exit(2)
        elif opt in ("-a", "--action"):
            if action != '':
                print('only one action per command')
                sys.exit(2)
            if (arg not in action_list):
                print('Error: the action ' + arg + ' has to be in that list: ' + str(action_list))
                sys.exit(2)
            action = arg
        elif opt in ("-f", "--format"):
            format = arg.lower()
            if (format != "csv" and format != "xlsx"):
                print('Erreur: le format doit être CSV ou XLSX, pas ' + arg)
                sys.exit(2)
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("--intersect"):
            intersect = True
        # elif opt in ("--brut"):
        #     intersectoptions = 'brut'
    if outputfile == '':
        print('Give an name for the output')
        sys.exit(1)
    print('Command to be executed (askquery): ', ' '.join(argv))
    format = utils.processargs(args, outputfile, format, printnotice)
    if intersect:
        newdf = load_and_intersect(args, intersectoptions)
        newdf.to_csv(outputfile, index=False)
        # if (format == "xlsx"):
        #     newdf.to_excel(outputfile, header=True, index=None)
        # else:
        #     newdf.to_csv(outputfile, index=False)
    else:
        for inputfile in args:
            try:
                print('Loading: ' + inputfile)
                if action == "fusion12":
                    paramfusion = 12
                elif action == "fusion34":
                    paramfusion = 34
                else:
                    paramfusion = 0
                (odf, ndf) = init_askquery_advanced(inputfile, paramfusion)
                # print(odf)
                # print(ndf)
                if action == "annot2ispart1":
                    newdf = value2_is_participant1(odf, ndf)
                elif action == "part1ispart2":
                    newdf = participant1_is_participant2(odf, ndf)
                elif action == "part1ispart3":
                    newdf = participant1_is_participant3(odf, ndf)
                elif action == "samepart":
                    newdf = same_participant(odf, ndf)
                elif action == "part1or2ispart3":
                    newdf = participant1or2_is_participant3(odf, ndf)
                elif action == "annot3ispart1or2":
                    newdf = value3_is_participant1or2(odf, ndf)
                elif action == "annot3ispart1and2andreverse":
                    newdf = value3_is_participant1and2_and_reverse(odf, ndf)
                elif action == "x_parle_a_y_et_x_regarde_y":
                    newdf = x_parle_a_y_et_x_regarde_y(odf, ndf)
                elif action == "y_parle_a_x_et_x_regarde_y":
                    newdf = y_parle_a_x_et_x_regarde_y(odf, ndf)
                elif action == "notiret":
                    newdf = notiret(odf, ndf)
                elif action == "lnaera123":
                    newdf = langage_non_adresse_enfant_regarde_adressee_123(odf, ndf)
                elif action == "lnaers123":
                    newdf = langage_non_adresse_enfant_regarde_speaker_123(odf, ndf)
                elif action == "lsre123":
                    newdf = langage_sans_reponse_enfant_123(odf, ndf)
                elif action == "fusion12":
                    newdf = replace_fusion12(odf, ndf)
                elif action == "fusion34":
                    newdf = replace_fusion34(odf, ndf)
                else:
                    print("unknown action: " + action)
            except KeyError:
                # exc = sys.exception()
                # print("*** print_tb:")
                # traceback.print_tb(exc.__traceback__, limit=1, file=sys.stdout)
                # print("*** print_exception:")
                # traceback.print_exception(exc, limit=2, file=sys.stdout)
                # print("*** print_exc:")
                traceback.print_exc()  # (limit=2, file=sys.stdout)
                print('Probably bad format for input file: ' + inputfile)
                pass
            except KeyboardInterrupt:
                sys.exit(1)
            except Exception as e:
                print('Probably bad input file or general error?')
                print(e)
                traceback.print_exc()
                sys.exit(6)

            if (format == "xlsx"):
                newdf.to_excel(outputfile, header=True, index=None)
            else:
                newdf.to_csv(outputfile, index=False)

    sys.exit(0)


if __name__ == "__main__":
    askquery(sys.argv[1:])
