python3 src/intersections.py exemples/FD-F4-DIN1-Complet-01-07-2022-Extend.eaf -o dinlang-vocal.csv -t vocal
python3 src/intersections.py exemples/DL-FRA1-DIN1-Complet-Extend-08-07-2022.eaf -o dinlang-vocal.csv -t vocal
python3 src/intersections.py exemples/SF-F2-DIN1-Complet-01-07-2022-Extend.eaf -o dinlang-visual.csv
python3 src/intersections.py exemples/SF-F4-DIN1-Complet-01-07-2022-Extend.eaf -o dinlang-visual.csv

# information temps de traitement
## temps non optimisé
# python3 test.py  5.17s user 0.56s system 134% cpu 4.262 total
## temps optimisé
# python3 test.py  1.50s user 0.73s system 463% cpu 0.481 total

