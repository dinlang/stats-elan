# stats-elan
Calcul de statistiques à partir de transcriptions ELAN

## but
Réaliser des recherches de correspondances entre tiers comme réalisées dans la recherche complexe de multiples fichiers

Version plus simple et plus rapide, pas besoin de lancer ELAN, peut-être automatisée et les fichiers résultats sont nettoyés des données inutiles que le calcul de ELAN génère quand il y a plus de deux tiers comparés.

Ce programme n'a pas la souplesse du logiciel ELAN, ou alors nécessite des développements complémentaires. Par contre, il a toute la liberté de développement d'un langage de programmation classique.

Conçu en Python en utlisant la bibliothèque pympi

## mise en oeuvre

### avec environnement

La première fois, créer un environnement python
```
python3 -m virtualenv elan
```

puis à chaque fois charger cet environnement
```
source elan/bin/activate
```
### En fin de session

```
deactivate
```

## utilisation dans un serveur

  - Ne pas se servir de l'environnement
  - Mettre les librairies dans le système (serveur)
  - Utiliser la même version de python (pour cela de préférence utiliser un chemin absolu) dans le chargement des bibliothèques et dans le code du serveur

## La première fois ou si des mises à jour de pympi sont disponibles (et de pandas)
```
pip install pympi-ling==1.70.2
pip install pandas
pip install openpyxl
```

## Commandes à utiliser:
```
python3 intersections.py <inputfiles ...> -o <outputfile> -t <type> -p <participant> -a

Usage:
intersections.py <inputfiles ...> -o <outputfile> -t <type> -p <participant> -a
   inputfiles = listes des fichiers à traiter
   -o outfile = fichier résultat au format csv
   -t type = type de calcul: visual => pour les familles signeuses et vocal => pour les familles entendantes
   -p participant = nom du participant: M, P, Ea, Eb, Ec
   -a = tous les participants
```


## install python libraries for everyone
```
umask 022
sudo -H pip3 install pympi-ling==1.70.2
sudo -H pip3 install pandas
```
