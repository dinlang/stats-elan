<?php

header("Content-Type: application/json");
header("Acess-Control-Allow-Origin: *");
header("Acess-Control-Allow-Methods: POST");
header("Acess-Control-Allow-Headers: Acess-Control-Allow-Headers,Content-Type,Acess-Control-Allow-Methods, Authorization");

error_reporting(E_ALL);

$python = "/usr/local/bin/python3";
#$python = "python3";

# get all parameters
$action = isset($_POST['action']) ? $_POST['action'] : "inter";
$motif = isset($_POST['motif']) ? $_POST['motif'] : "";
$pattern = isset($_POST['pattern']) ? $_POST['pattern'] : "exact";
$tiertype = isset($_POST['tiertype']) ? $_POST['tiertype'] : "tier";

$motif1 = isset($_POST['motif1']) ? $_POST['motif1'] : "";
$pattern1 = isset($_POST['pattern1']) ? $_POST['pattern1'] : "exact";
$tiertype1 = isset($_POST['tiertype1']) ? $_POST['tiertype1'] : "tier";
$motif2 = isset($_POST['motif2']) ? $_POST['motif2'] : "";
$pattern2 = isset($_POST['pattern2']) ? $_POST['pattern2'] : "exact";
$tiertype2 = isset($_POST['tiertype2']) ? $_POST['tiertype2'] : "tier";
$motif3 = isset($_POST['motif3']) ? $_POST['motif3'] : "";
$pattern3 = isset($_POST['pattern3']) ? $_POST['pattern3'] : "exact";
$tiertype3 = isset($_POST['tiertype3']) ? $_POST['tiertype3'] : "tier";
$motif4 = isset($_POST['motif4']) ? $_POST['motif4'] : "";
$pattern4 = isset($_POST['pattern4']) ? $_POST['pattern4'] : "exact";
$tiertype4 = isset($_POST['tiertype4']) ? $_POST['tiertype4'] : "tier";

$modality = isset($_POST['modality']) ? $_POST['modality'] : "";

$compl = isset($_POST['complements']) ? $_POST['complements'] : "none";

$format = isset($_POST['targetformat']) ? $_POST['targetformat'] : "csv";

$curdir = getcwd();
$help = isset($_POST['help']) ? $_POST['help'] : "";

if ($help == 'help') {
	echo json_encode(array("message" => "use: inputfile (form) + parameters: modality = vocal or visual", "status" => false));	
	exit(1);
}

# prepare the files

if (!isset($_FILES['inputfile']) || !isset($_FILES['inputfile']['name'])) {
	$errorMSG = json_encode(array("message" => "some unknow error in the files: was a file selected ?", "type" => $modality, "status" => false));	
	echo $errorMSG;
	exit(1);
}

if (empty($_FILES['inputfile']) || empty($_FILES['inputfile']['name']) || count($_FILES['inputfile']['name']) < 1) {
	$errorMSG = json_encode(array("message" => "please select an EAF or CSV file", "type" => $modality, "status" => false));	
	echo $errorMSG;
	exit(1);
}

# there are one or more files
$upload_path = "../data/inputfiles/"; // set upload folder path
$result_path = "../data/outputfiles/";

/**
 * Fixes the odd indexing of multiple file uploads from the format:
 *
 * $_FILES['field']['key']['index']
 *
 * To the more standard and appropriate:
 *
 * $_FILES['field']['index']['key']
 *
 * @param array $files
 * @author Corey Ballou
 * @link http://www.jqueryin.com
 */
function fixFilesArray(&$files)
{
    $names = array( 'name' => 1, 'type' => 1, 'tmp_name' => 1, 'error' => 1, 'size' => 1);

    foreach ($files as $key => $part) {
        // only deal with valid keys and multiple files
        $key = (string) $key;
        if (isset($names[$key]) && is_array($part)) {
            foreach ($part as $position => $value) {
                $files[$position][$key] = $value;
            }
            // remove old key reference
            unset($files[$key]);
        }
    }
}

# first create a result name using the first file name

if ($format == "xlsx") $ext = ".xlsx"; else $ext = ".csv";
$fileName0 = $_FILES['inputfile']['name'][0];
$fileNameEncoded0 = rawurlencode($fileName0);
$fileNameResultEncoded = pathinfo($fileNameEncoded0,PATHINFO_FILENAME).$ext;
$fileNameResult = pathinfo($fileName0,PATHINFO_FILENAME).$ext;
$joinedFilesToProcess = "";

#$total = count($_FILES['inputfile']['name']);
#for ($nthfile = 0; $nthfile < $total; $nthfile++) {

fixFilesArray($_FILES['inputfile']);
foreach ($_FILES['inputfile'] as $position => $blocfile) {
	// should output array with indices name, type, tmp_name, error, size
#	var_dump($blocfile);

#	$nthFileName  =  isset($_FILES['inputfile']['name'][$nthfile]) ? $_FILES['inputfile']['name'][$nthfile] : "";
#	$nthTempPath  =  isset($_FILES['inputfile']['tmp_name'][$nthfile]) ? $_FILES['inputfile']['tmp_name'][$nthfile] : "";
#	$nthFileSize  =  isset($_FILES['inputfile']['size'][$nthfile]) ? $_FILES['inputfile']['size'][$nthfile] : "";

	// $nthFileName  =  $_FILES['inputfile']['name'][$nthfile];
	// $nthTempPath  =  $_FILES['inputfile']['tmp_name'][$nthfile];
	// $nthFileSize  =  $_FILES['inputfile']['size'][$nthfile];
	$nthFileName  =  $blocfile['name'];
	$nthTempPath  =  $blocfile['tmp_name'];
	$nthFileSize  =  $blocfile['size'];
	$nthFileNameEncoded = rawurlencode($nthFileName);

	if (empty($nthTempPath) || empty($nthFileName)) {
		continue;
//		$errorMSG = json_encode(array("message" => "No temp file", "curdir" => $curdir, "nth" => $nthfile, "tmp" => $nthTempPath, "file" => $upload_path . $nthFileNameEncoded));
//		echo $errorMSG;
//		exit(1);
	}
	
	$nthFileExt = strtolower(pathinfo($nthFileName,PATHINFO_EXTENSION)); // get image extension
	// valid image extensions
	$valid_extensions = array('eaf', 'csv');
	// allow valid image file formats
	if (!in_array($nthFileExt, $valid_extensions)) {
		$errorMSG = json_encode(array("message" => "Sorry, only EAF or CSV files are allowed", "status" => false));	
		echo $errorMSG;
		exit(1);
	}

	// emplacement intermédiaire des fichiers $upload_path.$fileNameEncoded
	// a mettre à la fin dans $joinedFilesToProcess
	if (file_exists($upload_path . $nthFileNameEncoded)) {
		unlink($upload_path . $nthFileNameEncoded);
		// $errorMSG = json_encode(array("message" => "Sorry, file already exists check upload folder", "status" => false, "filename" => $upload_path . $nthFileNameEncoded));	
		// echo $errorMSG;
		// exit(1);
	}

	// check file size '200MB'
	if ($nthFileSize > 20 * 1024 * 1024) {
		$errorMSG = json_encode(array("message" => "Sorry, your file is too large, please upload max 20 MB size", "status" => false));	
		echo $errorMSG;
		exit(1);
	}

	$res = move_uploaded_file($nthTempPath, $upload_path . $nthFileNameEncoded); // move file from system temporary path to our upload folder path
	if ($res != true) {
		$errorMSG = json_encode(array("message" => "Cannot move uploaded file", "curdir" => $curdir, "nth" => $position, "tmp" => $nthTempPath, "file" => $upload_path . $nthFileNameEncoded));
		echo $errorMSG;
		exit(1);
	}

	$joinedFilesToProcess = $joinedFilesToProcess . " \"" . $upload_path . $nthFileNameEncoded . "\"";
}

		
// if no error caused, continue ....
if ($action == "coact") {
	$params = "-m audvis"; // default
	// if ($modality == "vocal") $params = "-m vocal ";
	if ($modality == "visual") $params = "-m visual";
	if ($modality == "vocal") $params = "-m vocal";
	if ($format != "") {
		$params = $params . " -f " . $format;
	}
}

// preparer les parametres

if ($action == "inter") {
	$params = ""; // default
	if ($motif1 != "") {
		if ($tiertype1 == "tier") {
			if ($pattern1 == "regexp") {
				$params .= " --pe " . $motif1;
			} else if ($pattern1 == "part") {
				$params .= " --pp " . $motif1;
			} else {
				$params .= " -p " . $motif1;
			}
		} else {
			if ($pattern1 == "regexp") {
				$params .= " --te " . $motif1;
			} else if ($pattern1 == "part") {
				$params .= " --tp " . $motif1;
			} else {
				$params .= " -t " . $motif1;
			}
		}
	}
	if ($motif2 != "") {
		if ($tiertype2 == "tier") {
			if ($pattern2 == "regexp") {
				$params .= " --pe " . $motif2;
			} else if ($pattern2 == "part") {
				$params .= " --pp " . $motif2;
			} else {
				$params .= " -p " . $motif2;
			}
		} else {
			if ($pattern2 == "regexp") {
				$params .= " --te " . $motif2;
			} else if ($pattern2 == "part") {
				$params .= " --tp " . $motif2;
			} else {
				$params .= " -t " . $motif2;
			}
		}
	}
	if ($motif3 != "") {
		if ($tiertype3 == "tier") {
			if ($pattern3 == "regexp") {
				$params .= " --pe " . $motif3;
			} else if ($pattern3 == "part") {
				$params .= " --pp " . $motif3;
			} else {
				$params .= " -p " . $motif3;
			}
		} else {
			if ($pattern3 == "regexp") {
				$params .= " --te " . $motif3;
			} else if ($pattern3 == "part") {
				$params .= " --tp " . $motif3;
			} else {
				$params .= " -t " . $motif3;
			}
		}
	}
	if ($motif4 != "") {
		if ($tiertype4 == "tier") {
			if ($pattern4 == "regexp") {
				$params .= " --pe " . $motif4;
			} else if ($pattern4 == "part") {
				$params .= " --pp " . $motif4;
			} else {
				$params .= " -p " . $motif4;
			}
		} else {
			if ($pattern4 == "regexp") {
				$params .= " --te " . $motif4;
			} else if ($pattern4 == "part") {
				$params .= " --tp " . $motif4;
			} else {
				$params .= " -t " . $motif4;
			}
		}
	}
	if ($format != "" && $compl == "none") {
		$params = $params . " -f " . $format;
	}
}

// on fait les traitements et il y a trois cas:
// seulement intersection (cas 3 ci dessous)
// seulement post traitement (cas 1 ci dessous)
// intersections et post traitement (cas 2 ci dessous)

// les fichiers d'entrée sont dans la variable $joinedFilesToProcess (il peut y en avoir un ou plusieurs et les noms sont séparés par des espaces)

			// $cmd = 'sh ../php/stats-elan-cmds.sh '.$upload_path.$fileName.' '.$result_path.$fileNameResultEncoded.' '.$params;
			// $res = shell_exec($cmd);
if ($compl != "none" && $action == "none") {
	if ($format != "") {
		$cmd2fmt = " -f " . $format;
	} else {
		$cmd2fmt = "";
	}
	// second command
	$cmd = $python." ./askquery.py -a ".$compl.$cmd2fmt." \"".$joinedFilesToProcess."\" -o \"".$result_path.$fileNameResultEncoded. "\"";
	$res2 = exec($cmd, $outputcmd2, $return_var);
	$res = "nothing || " . $res2;
	$outputcmd = "nothing || ". implode(",", $outputcmd2);
} else if ($compl != "none") {
	$tmpfname = tempnam("../data/outputfiles/", "STATSELAN");
	$cmd = $python." ./intersections-main.py ".$params." \"".$joinedFilesToProcess."\" -o \"".$tmpfname. "\"";
	// $cmd = $python." ./intersections-main.py ".$params.' "'.$joinedFilesToProcess.'" -o "'.$tmpfname. '"';
	// $cmd = $python." ./intersections-main.py ".$params." ".$joinedFilesToProcess." -o ".$tmpfname;
	$res = exec($cmd, $outputcmd, $return_var);
	if ($return_var == 0) {
		if ($format != "") {
			$cmd2fmt = " -f " . $format;
		} else {
			$cmd2fmt = "";
		}
		// second command
		$cmd = $python." ./askquery.py -a ".$compl.$cmd2fmt." \"".$tmpfname."\" -o \"".$result_path.$fileNameResultEncoded. "\"";
		$res2 = exec($cmd, $outputcmd2, $return_var);
		$res .= " || " . $res2;
		$outputcmd = implode(",", $outputcmd) . " || ". implode(",", $outputcmd2);
	} else {
		$res .= " || second res absent";
		$outputcmd = implode(",", $outputcmd) . " || second command not run";
	}
} else {
	$cmd = $python." ./intersections-main.py ".$params." ".$joinedFilesToProcess." -o \"".$result_path.$fileNameResultEncoded. "\"";
	// $cmd = $python." ./intersections-main.py ".$params.' "'.$joinedFilesToProcess.'" -o "'.$result_path.$fileNameResultEncoded. '"';
	// $cmd = $python." ./intersections-main.py ".$params." ".$joinedFilesToProcess." -o ".$result_path.$fileNameResultEncoded;
	$res = exec($cmd, $outputcmd, $return_var);
}

// fin des traitements et renvoi des résultats
if ($return_var == 0) {
	if (!empty($tmpfname) && file_exists($tmpfname)) unlink($tmpfname);
	$data = file_get_contents($result_path.$fileNameResultEncoded);
	if ($format === "xlsx") {
		$opResult = array("curdir" => $curdir, "command" => $cmd, "resultname" => $fileNameResult, 
			"format" => "xlsx", "outputcmd" => $outputcmd, "returnvar" => $return_var, "res" => $res, "status" => true,
			"tiertype" => $tiertype, "pattern" => $pattern,
			"data" => base64_encode($data));
		echo json_encode($opResult);
	} else {
		echo json_encode(array("curdir" => $curdir, "command" => $cmd, "data" => $data, "resultname" => $fileNameResult, 
		"tiertype" => $tiertype, "pattern" => $pattern,
		"format" => $format, "outputcmd" => $outputcmd, "returnvar" => $return_var, "res" => $res, "status" => true));
	}
} else {
	echo json_encode(array("curdir" => $curdir, "cmd" => $cmd, "fileNameResult", $fileNameResult,
		"message" => ("cannot process file: " . $joinedFilesToProcess . " erreur ".$return_var), "outputcmd" => $outputcmd, "returnvar" => $return_var, "res" => $res, "status" => false));
}

// $cmdshell = "conda activate devcorli && python "." ./intersections.py ".$params." \"".$joinedFilesToProcess."\" -o \"".$result_path.$fileNameResultEncoded. "\"";
// $shellres = shell_exec($cmdshell);
// $data = file_get_contents($result_path.$fileNameResultEncoded);
// if ($format === "xlsx") {
// 	$opResult = array("curdir" => $curdir, "command" => $cmd, "resultname" => $fileNameResult, 
// 		"format" => "xlsx", "res" => $res, "status" => true, 
// 		"data" => base64_encode($data));
// 	echo json_encode($opResult);
// } else {
// 	echo json_encode(array("curdir" => $curdir, "command" => $cmd, "data" => $data, "resultname" => $fileNameResult, 
// 		"format" => $format, "res" => $res, "status" => true));
// }

?>