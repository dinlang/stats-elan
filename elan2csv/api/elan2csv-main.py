import pympi
import pandas as pd
import os
import sys

sys.path.insert(1, "/applis/toolselan/bibl/python/")
#sys.path.insert(1, "/applis/dev/toolselan/bibl/python/")
#sys.path.insert(1, "/Users/cp/devlopt/elan-data-analysis/bibl/python/")

import utils as utils
import cleanemptytimes as cleanemptytimes

# get_ref_annotation_data_for_tier
# lng_vis = eaf_file.get_annotation_data_for_tier('lng-vis-' + participant)

def add_text(dest, ctn, motif):
	if (dest == ''):
		return ctn
	else:
		return dest + ' ' + motif + ' ' + ctn

def list_main_tiers(eaf_file):
	thelist = []
	tns = eaf_file.get_tier_names()
	for tn in tns:
		tnparam = eaf_file.get_parameters_for_tier(tn)
		if not 'PARENT_REF' in tnparam:
			thelist.append(tn)
	return thelist

def list_secondary_types(eaf_file):
	thelist = []
	tns = eaf_file.get_tier_names()
	for tn in tns:
		tnparam = eaf_file.get_parameters_for_tier(tn)
		if 'PARENT_REF' in tnparam:
			if not tnparam['LINGUISTIC_TYPE_REF'] in thelist: thelist.append(tnparam['LINGUISTIC_TYPE_REF'])
	return thelist

def filter_time(tmstart, tmend, listannot):
	filtered_list = []
	for e in listannot:
		if (e[0] < tmstart or e[0] >= tmend): continue
		if (e[1] > tmend): continue
		filtered_list.append(e)
	return filtered_list

def add_dep_tier(eaf_file, dep_tier, annotation, first_row, new_row, other_rows, nolinedup):
	param = eaf_file.get_parameters_for_tier(dep_tier)
	dep_tier_type = param['LINGUISTIC_TYPE_REF']
	type = eaf_file.get_parameters_for_linguistic_type(dep_tier_type)
	dep_tier_index = first_row.index(dep_tier_type)
	if (type['CONSTRAINTS'] == 'Symbolic_Association'):
		nona = filter_time(annotation[0], annotation[1], eaf_file.get_annotation_data_at_time(dep_tier, annotation[0]))
	else:
		nona = filter_time(annotation[0], annotation[1], eaf_file.get_annotation_data_between_times(dep_tier, annotation[0], annotation[1]))
	# print("===================================================================")
	# print(annotation)
	# print(annotation[0]+1)
	# print(annotation[1]-1)
	# print(dep_tier_type)
	# print(dep_tier)
	# print(eaf_file.get_annotation_data_for_tier(dep_tier))
	# print(nona)
	# +1 and -1 necessary otherwise more than the current annotation are selected (previous and next ones get selected)
	# print('loc', tn, 'deptier', dep_tier, nona)
	if (len(nona) >= 1):
		# find the correct type for dep_tier
		new_row[dep_tier_index] = add_text(new_row[dep_tier_index], nona[0][2], "§")
		for ictn in range(1, len(nona)):
			# print('divisions')
			if (nolinedup == True):
				new_row[dep_tier_index] = add_text(new_row[dep_tier_index], nona[ictn][2], "§§")
			else:
				# print('linedup')
				if (len(other_rows) <= ictn-1):
					# create new other row
					# print('create new other row: ' + str(ictn-1))
					new_other_row = [''] * len(first_row)
					new_other_row[0] = new_row[0]
					new_other_row[1] = new_row[1]
					new_other_row[2] = new_row[2]
					new_other_row[3] = new_row[3]
					new_other_row[4] = 'Duplicate'
					other_rows.append(new_other_row)
				other_rows[ictn-1][dep_tier_index] = add_text(other_rows[ictn-1][dep_tier_index], nona[ictn][2], "§§§")
				# print('result new rows')
				# print(other_rows)

def process_file(inputfile, outputfile, style, format, nolinedup=False):
	# test if input file exists
	if (not os.path.isfile(inputfile)):
		print("the inputfile " + inputfile + " does not exist")
		sys.exit(5)
	cleanemptytimes.cleanemptytimes([inputfile])
	if (outputfile == ''):
		pathname, extension = os.path.splitext(inputfile)
		if (format == "xlsx"):
			outputfile = pathname + '.xlsx'
		else:
			outputfile = pathname + '.csv'
	# opening the .eaf file
	eaf_file = pympi.Elan.Eaf(inputfile)
	# fill big table
	tier_names = eaf_file.get_tier_names()
	table = []

	if (style != ''):
		# list all orthography tiers
		target_tiers = list(eaf_file.get_tier_ids_for_linguistic_type(style))
		target_tiers.sort()
		# and all non orthography tiers
		non_target_types = list(eaf_file.get_linguistic_type_names())
		non_target_types.sort()
		if (style not in non_target_types):
			print('The main style (' + style + ') does not exist in the file.')
			sys.exit(4)
		non_target_types.remove(style)
		# create the first row of the table
		if (nolinedup == False):
			first_row = ['loc', 'start', 'end', style, 'Duplicate']
		else:
			first_row = ['loc', 'start', 'end', style]
	else:
		# list all orthography tiers
		target_tiers = list_main_tiers(eaf_file)
		target_tiers.sort()
		# print(target_tiers)
		# and all non orthography tiers
		non_target_types = list_secondary_types(eaf_file)
		non_target_types.sort()
		# print(non_target_types)
		# create the first row of the table
		if (nolinedup == False):
			first_row = ['loc', 'start', 'end', 'maintier', 'Duplicate']
		else:
			first_row = ['loc', 'start', 'end', 'maintier']

	for t in non_target_types:
		first_row.append(t)
	# create rows for all the data
	for tn in target_tiers:
		annots = eaf_file.get_annotation_data_for_tier(tn)
		for annot in annots:
			# create empty list
			new_row = [''] * len(first_row)
			# first the content of the orthography annnotation itself
			new_row[0] = tn
			new_row[1] = annot[0]
			new_row[2] = annot[1]
			new_row[3] = annot[2]
			if (nolinedup == False): new_row[4] = ''
			other_rows = [] # supplementary elements for secondary rows
			# then for all the depending tiers, add them in a column according to their type name
			dep = eaf_file.get_child_tiers_for(tn)
			for dep_tier in dep:
				add_dep_tier(eaf_file, dep_tier, annot, first_row, new_row, other_rows, nolinedup)
				dep_dep_tier = eaf_file.get_child_tiers_for(dep_tier)
				for ddt in dep_dep_tier:
					add_dep_tier(eaf_file, ddt, annot, first_row, new_row, other_rows, nolinedup)
			table.append(new_row)
			# duplicate elements in duplicated rows
			for r in range(1, len(new_row)):
				for t in other_rows:
					if (t[r] == ''): t[r] = new_row[r]
			# now add the duplicated rows
			for t in other_rows:
				table.append(t)

	df = pd.DataFrame(table, columns=first_row)
	# erase outputfile
	if (os.path.isfile(outputfile)):
		os.unlink(outputfile)
	# Sauvegarde de la dataframe au format .csv
	# if file does not exist write header
	if (format == "xlsx"):
		df.to_excel(outputfile, header=True, index=None)
	else:
		df.to_csv(outputfile, header='column_names', index=False)

import sys, getopt

def printnotice():
	print('elan2csv_chatstyle.py <inputfile ...> -o <outputfile> [-m -t <type> -h]')

def printhelp():
	print('elan2csv_chatstyle.py <inputfile ...> -o <outputfile> [-m -t <type> -h]')
	print('   inputfile = listes des fichiers à traiter')
	print('   -o outfile = fichier résultat au format csv')
	print('   -m = main lines are all main tiers')
	print('   -l = lines with an element division are put on the same line with a § (otherwise they are duplicated)')
	print('   -t type = "type" is the only main line (and main tiers)')
	print('       by default "orthography is the main line type')
	print('   -h = this help')

def main(argv):
	outputfile = ''
	style = 'orthography' # default
	format = ""
	nolinedup = False
	try:
		opts, args = getopt.gnu_getopt(argv,"o:mt:hf:l",["ofile=","main","type=","help", "format=", "sameline"])
	except getopt.GetoptError:
		printnotice()
		sys.exit(3)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			printhelp()
			sys.exit(1)
		elif opt in ("-m", "--main"):
			style = ''
		elif opt in ("-t", "--type"):
			style = arg
		elif opt in ("-l", "--sameline"):
			nolinedup = True
		elif opt in ("-f", "--format"):
			format = arg.lower()
			if (format != "csv" and format != "xlsx"):
				print('Erreur: le format doit être CSV ou XLSX, pas ' + arg)
				sys.exit(2)
		elif opt in ("-o", "--ofile"):
			outputfile = arg
	format = utils.processargs(args, outputfile, format, printnotice)
	for inputfile in args:
		try:
			process_file(inputfile, outputfile, style, format, nolinedup)
		except KeyError:
			print('Probably bad format for input file: ' + inputfile)
			pass
		except KeyboardInterrupt:
			sys.exit(1)
		# except:
		# 	print('Probably bad input file')
		# 	sys.exit(6)
	sys.exit(0)

if __name__ == "__main__":
   main(sys.argv[1:])
