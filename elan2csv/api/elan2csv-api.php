<?php

header("Content-Type: application/json");
header("Acess-Control-Allow-Origin: *");
header("Acess-Control-Allow-Methods: POST");
header("Acess-Control-Allow-Headers: Acess-Control-Allow-Headers,Content-Type,Acess-Control-Allow-Methods, Authorization");

error_reporting(E_ALL);

$python = "/usr/bin/python3";

$fileName  =  isset($_FILES['inputfile']) && isset($_FILES['inputfile']['name']) ? $_FILES['inputfile']['name'] : "";
$tempPath  =  isset($_FILES['inputfile']) && isset($_FILES['inputfile']['tmp_name']) ? $_FILES['inputfile']['tmp_name'] : "";
$fileSize  =  isset($_FILES['inputfile']) && isset($_FILES['inputfile']['size']) ? $_FILES['inputfile']['size'] : "";
$type = isset($_POST['type']) ? $_POST['type'] : "";
$main = isset($_POST['main']) ? $_POST['main'] : "";
$subline = isset($_POST['subline']) ? $_POST['subline'] : "";
$format = isset($_POST['targetformat']) ? $_POST['targetformat'] : "csv";
$curdir = getcwd();
$help = isset($_POST['help']) ? $_POST['help'] : "";

$fileNameEncoded = rawurlencode($fileName);

if ($help == 'help') {
	echo json_encode(array("message" => "use: inputfile (form) + parameters: type ; main ; subline ; format", "status" => false));	
	exit(0);
}

if (empty($fileName)) {
	$errorMSG = json_encode(array("message" => "please select an EAF file", "type" => $type, "status" => false));	
	echo $errorMSG;
} else {
	$upload_path = "../data/inputfiles/"; // set upload folder path
	$result_path = "../data/outputfiles/";

	if ($format == "xlsx") $ext = ".xlsx"; else $ext = ".csv";
	$fileNameResultEncoded = pathinfo($fileNameEncoded,PATHINFO_FILENAME).$ext;
	$fileNameResult = pathinfo($fileName,PATHINFO_FILENAME).$ext;
	
	$fileExt = strtolower(pathinfo($fileName,PATHINFO_EXTENSION)); // get image extension
	// valid image extensions
	$valid_extensions = array('eaf');
					
	// allow valid image file formats
	if(in_array($fileExt, $valid_extensions)) {
		if(file_exists($upload_path . $fileName)) unlink($upload_path . $fileName);

		//check file not exist our upload folder path
		if(!file_exists($upload_path . $fileName)) {
			// check file size '200MB'
			if ($fileSize < 200 * 1024 * 1024) {
				$res = move_uploaded_file($tempPath, $upload_path . $fileNameEncoded); // move file from system temporary path to our upload folder path
				if ($res != true) {
					$errorMSG = json_encode(array("message" => "Cannot move uploaded file", "curdir" => $curdir, "tmp" => $tempPath, "file" => $upload_path . $fileNameEncoded));
					echo $errorMSG;
				}
			} else {
				$errorMSG = json_encode(array("message" => "Sorry, your file is too large, please upload 5 MB size", "status" => false));	
				echo $errorMSG;
			}
		} else {		
			$errorMSG = json_encode(array("message" => "Sorry, file already exists check upload folder", "status" => false));	
			echo $errorMSG;
		}
	} else {		
		$errorMSG = json_encode(array("message" => "Sorry, only EAF files are allowed", "status" => false));	
		echo $errorMSG;		
	}
}

// if no error caused, continue ....
if (!isset($errorMSG)) {
	if ($main == "main") {
		$params = "-m";
	} else if ($main == "ortho") {
		$params = "";
	} else if ($main == "choice" && $type != "") {
		$params = "-t " . $type;
	} else {
		$params = ""; // default
	}
	if ($format != "") {
		$params = $params . " -f " . $format;
	}
	if ($subline == "nosubline") {
		$params = $params . " -l "; // no line duplication
	}

	$cmd = $python." ./elan2csv-main.py ".$params." \"".$upload_path.$fileNameEncoded."\" -o \"".$result_path.$fileNameResultEncoded."\"";
	$res = exec($cmd, $outputcmd, $return_var);

	if ($return_var == 0) {
		$data = file_get_contents($result_path.$fileNameResultEncoded);
		if ($format === "xlsx") {
			$opResult = array("curdir" => $curdir, "command" => $cmd, "resultname" => $fileNameResult, "format" => "xlsx", "outputcmd" => $outputcmd, 
				"returnvar" => $return_var, "res" => $res, "status" => true, "data" => base64_encode($data));
			echo json_encode($opResult);
		} else {
			echo json_encode(array("curdir" => $curdir, "command" => $cmd, "data" => $data, "resultname" => $fileNameResult, "format" => $format, 
				"outputcmd" => $outputcmd, "returnvar" => $return_var, "res" => $res, "status" => true));
		}
	} else {
		echo json_encode(array("curdir" => $curdir, "command" => $cmd, "message" => ("cannot process file: " . $fileName . " erreur ".$return_var), "format" => $format, 
			"outputcmd" => $outputcmd, "returnvar" => $return_var, "res" => $res, "status" => false));
	}
}

?>