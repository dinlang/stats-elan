// form.js
const formId = "statselanform"; // ID of the form
const url = location.href; //  href for the page
const formIdentifier = `${url} ${formId}`; // Identifier used to identify the form
// const saveButton = document.querySelector("#save"); // select save button
// const alertBox = document.querySelector(".alert"); // select alert display div
let form = document.querySelector(`#${formId}`); // select form
let formElements = form.elements; // get the elements in the form

function getCheck(name) {
  let chk = document.getElementsByName(name);
  for (const element of chk) {
    if (element.checked) {
      return element.value;
    }
  }
  return "";
}

function setCheck(name, choice) {
  let chk = document.getElementsByName(name);
  for (const element of chk) {
    if (element.value == choice) {
      element.checked = true;
    } else {
      element.checked = false;
    }
  }
}

/**
 * This function gets the values in the form
 * and returns them as an object with the
 * [formIdentifier] as the object key
 * @returns {Object}
 */
const getFormData = () => {
  let data = { [formIdentifier]: {} };
  // data[formIdentifier]["motif"] = formElements.valeurmotif.value;
  data[formIdentifier]["motif1"] = formElements.valeurmotif1.value;
  data[formIdentifier]["motif2"] = formElements.valeurmotif2.value;
  data[formIdentifier]["motif3"] = formElements.valeurmotif3.value;
  data[formIdentifier]["motif4"] = formElements.valeurmotif4.value;

  // data[formIdentifier]["pattern"] = getCheck("pattern");
  // data[formIdentifier]["tiertype"] = getCheck("tiertype");
  data[formIdentifier]["pattern1"] = getCheck("pattern1");
  data[formIdentifier]["tiertype1"] = getCheck("tiertype1");
  data[formIdentifier]["pattern2"] = getCheck("pattern2");
  data[formIdentifier]["tiertype2"] = getCheck("tiertype2");
  data[formIdentifier]["pattern3"] = getCheck("pattern3");
  data[formIdentifier]["tiertype3"] = getCheck("tiertype3");
  data[formIdentifier]["pattern4"] = getCheck("pattern4");
  data[formIdentifier]["tiertype4"] = getCheck("tiertype4");

  data[formIdentifier]["modality"] = getCheck("modality");
  data[formIdentifier]["targetformat"] = getCheck("targetformat");

  data[formIdentifier]["postaction"] = document.getElementById("postaction").value;
  data[formIdentifier]["complements"] = getCheck("complements");
  return data;
};

const saveForm = () => {
  data = getFormData();
  localStorage.setItem(formIdentifier, JSON.stringify(data[formIdentifier]));
  const message = "Form draft has been saved!";
  // displayAlert(message);
  console.log(message);
};

/**
 * This function displays a message
 * on the page for 1 second
 *
 * @param {String} message
 */
// const displayAlert = message => {
//   alertBox.innerText = message;
//   alertBox.style.display = "block";
//   setTimeout(function() {
//     alertBox.style.display = "none";
//   }, 1000);
// };

/**
 * This function populates the form
 * with data from localStorage
 *
 */
const populateForm = () => {
  console.log('populate:', formId);
  document.getElementById("inputfile").value = "";
  if (localStorage.key(formIdentifier)) {
    const savedData = JSON.parse(localStorage.getItem(formIdentifier)); // get and parse the saved data from localStorage
    console.log(savedData);
    if (!savedData) return;
    // formElements.valeurmotif.value = savedData["motif"];
    formElements.valeurmotif1.value = savedData["motif1"];
    formElements.valeurmotif2.value = savedData["motif2"];
    formElements.valeurmotif3.value = savedData["motif3"];
    formElements.valeurmotif4.value = savedData["motif4"];

    // if (savedData["pattern"]) setCheck("pattern", savedData["pattern"]);
    // if (savedData["tiertype"]) setCheck("tiertype", savedData["tiertype"]);
    if (savedData["pattern1"]) setCheck("pattern1", savedData["pattern1"]);
    if (savedData["tiertype1"]) setCheck("tiertype1", savedData["tiertype1"]);
    if (savedData["pattern2"]) setCheck("pattern2", savedData["pattern2"]);
    if (savedData["tiertype2"]) setCheck("tiertype2", savedData["tiertype2"]);
    if (savedData["pattern3"]) setCheck("pattern3", savedData["pattern3"]);
    if (savedData["tiertype3"]) setCheck("tiertype3", savedData["tiertype3"]);
    if (savedData["pattern4"]) setCheck("pattern4", savedData["pattern4"]);
    if (savedData["tiertype4"]) setCheck("tiertype4", savedData["tiertype4"]);
  
    if (savedData["modality"]) setCheck("modality", savedData["modality"]);
    if (savedData["targetformat"]) setCheck("targetformat", savedData["targetformat"]);

    if (savedData["complements"]) setCheck("complements", savedData["complements"]);

    actionchoice(savedData["postaction"]);

    const message = "Form has been refilled with saved data!";
    // displayAlert(message);
    console.log(message);
  }
};

document.onload = populateForm(); // populate the form when the document is loaded
