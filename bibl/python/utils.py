# Utils for main programs
# v 0.0.1

import sys

def processargs(args, outputfile, format, printnotice):
	if len(args) < 1:
		print('Erreur: pas de fichier à traiter')
		printnotice()
		sys.exit(2)
	if (outputfile.lower().endswith(".xlsx")):
		if (format.lower() == "csv"):
			print('incompatibité en extension de fichier .xlsx et format csv')
			printnotice()
			sys.exit(2)
		format = "xlsx"
	elif (outputfile.lower().endswith(".csv")):
		if (format.lower() == "xlsx"):
			print('incompatibité entre extension de fichier .csv et format xlsx')
			printnotice()
			sys.exit(2)
		format = "csv"
	return format
