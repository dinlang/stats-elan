# compute.py
# v 0.0.1

def intersection2(tab1, tab2, participant, tab1tier, tab1type, tab2tier, tab2type, filename): #, tab1type, tab2type):
	result = []
	c1 = 0
	c2 = 0
	for c1 in range(len(tab1)):
		for c2 in range(len(tab2)):
			if (tab2[c2][1] < tab1[c1][0]): continue
			if (tab2[c2][0] > tab1[c1][1]): break
			#memorize last starting comparison
			#print("lng:", tab1[c1], tab2[c2])
			begin = max(tab1[c1][0], tab2[c2][0])
			end = min(tab1[c1][1], tab2[c2][1])
			#print("mm:", begin, end)
			dur1 = tab1[c1][1] - tab1[c1][0]
			dur2 = tab2[c2][1] - tab2[c2][0]
			durinter = end - begin
			if (begin < end):
				#print("append", (tab1[c1][0], tab1[c1][1], tab1[c1][2], tab2[c2][0], tab2[c2][1], tab2[c2][2]))
				result.append((tab1[c1][0], tab1[c1][1], dur1, tab1[c1][2], participant, tab1tier, tab1type, 
					tab2[c2][0], tab2[c2][1], dur2, tab2[c2][2], participant, tab2tier, tab2type, 
					begin, end, durinter, filename))
	return result

def intersection3(tab1, tab2, tab3, participant, tab1tier, tab1type, tab2tier, tab2type, tab3tier, tab3type, filename):
	result = []
	c1 = 0
	c2 = 0
	c3 = 0
	for c1 in range(len(tab1)):
		for c2 in range(len(tab2)):
			if (tab2[c2][1] < tab1[c1][0]): continue
			if (tab2[c2][0] > tab1[c1][1]): break
			for c3 in range(len(tab3)):
				if (tab3[c3][1] < tab2[c2][0]): continue
				if (tab3[c3][0] > tab2[c2][1]): break
				#memorize last starting comparison
				#print("lng:", tab1[c1], tab2[c2], tab3[c3])
				begin = max(tab1[c1][0], tab2[c2][0], tab3[c3][0])
				end = min(tab1[c1][1], tab2[c2][1], tab3[c3][1])
				#print("mm:", begin, end)
				dur1 = tab1[c1][1] - tab1[c1][0]
				dur2 = tab2[c2][1] - tab2[c2][0]
				dur3 = tab3[c3][1] - tab3[c3][0]
				durinter = end - begin
				if (begin < end):
					#print("append", (tab1[c1][0], tab1[c1][1], tab1[c1][2], tab2[c2][0], tab2[c2][1], tab2[c2][2], tab3[c3][0], tab3[c3][1], tab3[c3][2]))
					result.append((tab1[c1][0], tab1[c1][1], dur1, tab1[c1][2], participant, tab1tier, tab1type, 
						tab2[c2][0], tab2[c2][1], dur2, tab2[c2][2], participant, tab2tier, tab2type, 
						tab3[c3][0], tab3[c3][1], dur3, tab3[c3][2], participant, tab3tier, tab3type, 
						begin, end, durinter, filename))
	return result

def intersection4(tab1, tab2, tab3, tab4, participant, tab1tier, tab1type, tab2tier, tab2type, tab3tier, tab3type, tab4tier, tab4type, filename):
	result = []
	c1 = 0
	c2 = 0
	c3 = 0
	c4 = 0
	for c1 in range(len(tab1)):
		for c2 in range(len(tab2)):
			if (tab2[c2][1] < tab1[c1][0]): continue
			if (tab2[c2][0] > tab1[c1][1]): break
			for c3 in range(len(tab3)):
				if (tab3[c3][1] < tab2[c2][0]): continue
				if (tab3[c3][0] > tab2[c2][1]): break
				for c4 in range(len(tab4)):
					if (tab4[c4][1] < tab3[c3][0]): continue
					if (tab4[c4][0] > tab3[c3][1]): break
					#memorize last starting comparison
					#print("lng:", tab1[c1], tab2[c2], tab3[c3], tab3[c4])
					begin = max(tab1[c1][0], tab2[c2][0], tab3[c3][0], tab4[c4][0])
					end = min(tab1[c1][1], tab2[c2][1], tab3[c3][1], tab4[c4][1])
					#print("mm:", begin, end)
					dur1 = tab1[c1][1] - tab1[c1][0]
					dur2 = tab2[c2][1] - tab2[c2][0]
					dur3 = tab3[c3][1] - tab3[c3][0]
					dur4 = tab4[c4][1] - tab4[c4][0]
					durinter = end - begin
					if (begin < end):
						#print("append", (tab1[c1][0], tab1[c1][1], tab1[c1][2], tab2[c2][0], tab2[c2][1], tab2[c2][2], tab3[c3][0], tab3[c3][1], tab3[c3][2], tab3[c4][0], tab3[c4][1], tab3[c4][2]))
						result.append((tab1[c1][0], tab1[c1][1], dur1, tab1[c1][2], participant, tab1tier, tab1type, 
							tab2[c2][0], tab2[c2][1], dur2, tab2[c2][2], participant, tab2tier, tab2type, 
							tab3[c3][0], tab3[c3][1], dur3, tab3[c3][2], participant, tab3tier, tab3type, 
							tab3[c4][0], tab4[c4][1], dur4, tab4[c4][2], participant, tab4tier, tab4type, 
							begin, end, durinter, filename))
	return result
