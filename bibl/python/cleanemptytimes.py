import xml.etree.ElementTree as ET
import os
import sys, getopt

def printnotice():
	print('cleanemptytimes.py <inputfiles ...>')
	print('    Process all files (ELAN files) to remove unwanted timeline elements with empty times.')

def cleanemptytimes_onefile(filename):
	# test if input file exists
	if (not os.path.isfile(filename)):
		print("cleanemptytimes: the file " + filename + " does not exist")
		sys.exit(5)
	file_name, file_extension = os.path.splitext(filename)
	if (file_extension != ".eaf"):
		print('Only EAF files: ' + filename + " does not end if '.eaf'")
		return ""
	xmltree = ET.parse(filename)
	root = xmltree.getroot()
	prec = 0
	modif = False
	for time_slot in root.iter('TIME_SLOT'):
		tm = time_slot.get('TIME_VALUE', '-1')
		if (int(tm) < 0):
			time_slot.set('TIME_VALUE', prec)
			modif = True
		else:
			prec = tm
	if (modif == True):
		replace_file_name = file_name + '-withoktimeslot.eaf'
		xmltree.write(replace_file_name)
		return replace_file_name
	else:
		return ""
	
def cleanemptytimes(argv):
	try:
		opts, args = getopt.gnu_getopt(argv,"h", ["help"])
	except getopt.GetoptError:
		printnotice()
		sys.exit(3)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			printnotice()
			sys.exit(2)
	for inputfile in args:
		# test if input file exists
		if (not os.path.isfile(inputfile)):
			print("cleanemptytimes: the file " + inputfile + " does not exist")
			sys.exit(5)
		file_name, file_extension = os.path.splitext(inputfile)
		if (file_extension != ".eaf"):
			print('Only EAF files: ' + inputfile + " does not end if '.eaf'")
			continue
		xmltree = ET.parse(inputfile)
		root = xmltree.getroot()
		prec = 0
		modif = False
		for time_slot in root.iter('TIME_SLOT'):
			tm = time_slot.get('TIME_VALUE', '-1')
			if (int(tm) < 0):
				time_slot.set('TIME_VALUE', prec)
				modif = True
			else:
				prec = tm
		if (modif == True):
			replace_file_name = file_name + '-withbadtimeslot.eaf.bad'
			os.rename(inputfile, replace_file_name)
			print('The file ' + inputfile + " was corrected and the old file is now named " + replace_file_name)
			xmltree.write(inputfile)
		# else:
		# 	print('The file ' + inputfile + " does not need to be corrected")

if __name__ == "__main__":
   cleanemptytimes(sys.argv[1:])
