# tableaux utilisés pour les intersections
# tab[CT_ANNOT] = annot
# tab[CT_BEGIN] = debut
# tab[CT_END] = fin
# tab[CT_DUR] = dur
# tab[CT_TIER] = tier
# tab[CT_TYPE] = type
# tab[CT_PART] = participant
# tab[CT_FILENAME] = filename

import operator

CT_ANNOT = 0
CT_BEGIN = 1
CT_END = 2
CT_DUR = 3
CT_TIER = 4
CT_TYPE = 5
CT_PART = 6
CT_FILENAME = 7

import numpy as np


def fusion12(tab1, tab2):
	tabres = []
	for c1 in range(len(tab1)):
		for c2 in range(len(tab2)):
			# [CT_PART] participant
			# [CT_ANNOT] value
			if tab1[c1][CT_PART] != tab2[c2][CT_PART]:
				continue
			#if (tab2[c2][CT_END] < tab1[c1][CT_BEGIN]): continue
			#if (tab2[c2][CT_BEGIN] > tab1[c1][CT_END]): break
			# memorize last starting comparison
			# print("lng:", tab1[c1], tab2[c2])
			begin = max(tab1[c1][CT_BEGIN], tab2[c2][CT_BEGIN])
			end = min(tab1[c1][CT_END], tab2[c2][CT_END])
			# print("mm:", begin, end)
			durinter = end - begin
			if (begin < end):
				# only if intersection
				# print(tab1[c1][CT_PART] + " et " + tab2[c2][CT_PART])
				# print( str(c1) + " et " + str(c2) )
				tabelt = list(tab1[c1])
				if tab1[c1][CT_ANNOT] == tab2[c2][CT_ANNOT]:
					tabelt[CT_ANNOT] = tab1[c1][CT_ANNOT]
					tabelt[CT_TIER] = tab1[c1][CT_TIER] + '|' + tab2[c2][CT_TIER]
				elif tab1[c1][CT_ANNOT] == "--":
					tabelt[CT_ANNOT] = tab2[c2][CT_ANNOT]
					tabelt[CT_TIER] = tab2[c2][CT_TIER]
				elif tab2[c2][CT_ANNOT] == "--":
					tabelt[CT_ANNOT] = tab1[c1][CT_ANNOT]
					tabelt[CT_TIER] = tab1[c1][CT_TIER]
				elif tab1[c1][CT_ANNOT] != tab2[c2][CT_ANNOT]:
					tabelt[CT_TIER] = tab1[c1][CT_TIER] + '|' + tab2[c2][CT_TIER]
					tabelt[CT_ANNOT] = 'autre'
				tabelt[CT_BEGIN] = begin
				tabelt[CT_END] = end
				tabelt[CT_DUR] = durinter
				tabres.append(tuple(tabelt))
	return tabres


def intersection2simple_slow(tab1, tab2):
	result = []
	c1 = 0
	c2 = 0
	# print(tab1)
	# print(tab2)
	for c1 in range(len(tab1)):
		if c1 % 100 == 0:
			print(c1)
		for c2 in range(len(tab2)):
			# print("A: " + str(tab1[c1]) + "        B: " + str(tab2[c2]))
			if (tab1[c1][CT_FILENAME] != tab2[c2][CT_FILENAME]): continue  # not same file
			# if (tab1[c1][CT_PART] != tab2[c2][CT_PART]): continue # not same participant
			#if (tab2[c2][CT_END] < tab1[c1][CT_BEGIN]): continue
			#if (tab2[c2][CT_BEGIN] > tab1[c1][CT_END]): break
			# memorize last starting comparison
			# print("lng:", tab1[c1], tab2[c2])
			begin = max(tab1[c1][CT_BEGIN], tab2[c2][CT_BEGIN])
			end = min(tab1[c1][CT_END], tab2[c2][CT_END])
			# print("mm:", begin, end)
			durinter = end - begin
			if (begin < end):
				result.append((
					tab1[c1][CT_ANNOT], tab1[c1][CT_BEGIN], tab1[c1][CT_END], tab1[c1][CT_DUR], tab1[c1][CT_TIER],
					tab1[c1][CT_TYPE], tab1[c1][CT_PART],
					tab2[c2][CT_ANNOT], tab2[c2][CT_BEGIN], tab2[c2][CT_END], tab2[c2][CT_DUR], tab2[c2][CT_TIER],
					tab2[c2][CT_TYPE], tab2[c2][CT_PART],
					begin, end, durinter, tab1[c1][CT_FILENAME]))
	return result

#
# def intersection2simple_fast(tab1, tab2):
# 	result = []
# 	c1 = 0
# 	c2 = 0
# 	# print(tab1)
# 	# print(tab2)
# 	c1range = len(tab1)
# 	c2range = len(tab2)
# 	c1 = 0
# 	c2start = 0
# 	while c1 < c1range:
# 		c2 = c2start
# 		while c2 < c2range:
# 			# print("A: " + str(tab1[c1]) + "        B: " + str(tab2[c2]))
# 			if tab1[c1][CT_FILENAME] != tab2[c2][CT_FILENAME]:
# 				print('Two record with different filename: should not happen !')
# 				c2 = c2 + 1
# 				continue  # not same file
# 			# if (tab1[c1][CT_PART] != tab2[c2][CT_PART]): continue # not same participant
# 			if tab2[c2][CT_END] < tab1[c1][CT_BEGIN]:
# 				c2 = c2 + 1
# 				continue
# 			if tab2[c2][CT_BEGIN] > tab1[c1][CT_END]:
# 				c2start = c2
# 				break
# 			# memorize last starting comparison
# 			# print("lng:", tab1[c1], tab2[c2])
# 			begin = max(tab1[c1][CT_BEGIN], tab2[c2][CT_BEGIN])
# 			end = min(tab1[c1][CT_END], tab2[c2][CT_END])
# 			# print("mm:", begin, end)
# 			durinter = end - begin
# 			if begin < end:
# 				result.append((
# 					tab1[c1][CT_ANNOT], tab1[c1][CT_BEGIN], tab1[c1][CT_END], tab1[c1][CT_DUR], tab1[c1][CT_TIER],
# 					tab1[c1][CT_TYPE], tab1[c1][CT_PART],
# 					tab2[c2][CT_ANNOT], tab2[c2][CT_BEGIN], tab2[c2][CT_END], tab2[c2][CT_DUR], tab2[c2][CT_TIER],
# 					tab2[c2][CT_TYPE], tab2[c2][CT_PART],
# 					begin, end, durinter, tab1[c1][CT_FILENAME]))
# 			c2 = c2 + 1
# 		c1 = c1 + 1
# 	return result


def intersection3simple(tab1, tab2, tab3):
	result = []
	c1 = 0
	c2 = 0
	c3 = 0
	# print(tab1)
	# print(tab2)
	# print(tab3)
	for c1 in range(len(tab1)):
		if c1 % 100 == 0:
			print(c1)
		for c2 in range(len(tab2)):
			if (tab1[c1][CT_FILENAME] != tab2[c2][CT_FILENAME]): continue  # not same file
			# if (tab1[c1]CT_PARTICIPANT != tab2[c2][CT_PART]): continue # not same participant
			#if (tab2[c2][CT_END] < tab1[c1][CT_BEGIN]): continue
			#if (tab2[c2][CT_BEGIN] > tab1[c1][CT_END]): break
			for c3 in range(len(tab3)):
				if (tab1[c1][CT_FILENAME] != tab3[c3][CT_FILENAME]): continue  # not same file
				# if (tab1[c1][CT_PART] != tab3[c3][CT_PART]): continue # not same participant
				#if (tab3[c3][CT_END] < tab2[c2][CT_BEGIN]): continue
				#if (tab3[c3][CT_BEGIN] > tab2[c2][CT_END]): break
				# memorize last starting comparison
				# print("lng:", tab1[c1], tab2[c2], tab3[c3])
				begin = max(tab1[c1][CT_BEGIN], tab2[c2][CT_BEGIN], tab3[c3][CT_BEGIN])
				end = min(tab1[c1][CT_END], tab2[c2][CT_END], tab3[c3][CT_END])
				# print("mm:", begin, end)
				durinter = end - begin
				if (begin < end):
					result.append((
						tab1[c1][CT_ANNOT], tab1[c1][CT_BEGIN], tab1[c1][CT_END], tab1[c1][CT_DUR], tab1[c1][CT_TIER],
						tab1[c1][CT_TYPE], tab1[c1][CT_PART],
						tab2[c2][CT_ANNOT], tab2[c2][CT_BEGIN], tab2[c2][CT_END], tab2[c2][CT_DUR], tab2[c2][CT_TIER],
						tab2[c2][CT_TYPE], tab2[c2][CT_PART],
						tab3[c3][CT_ANNOT], tab3[c3][CT_BEGIN], tab3[c3][CT_END], tab3[c3][CT_DUR], tab3[c3][CT_TIER],
						tab3[c3][CT_TYPE], tab3[c3][CT_PART],
						begin, end, durinter, tab1[c1][CT_FILENAME]))
	return result


def intersection4simple(tab1, tab2, tab3, tab4):
	result = []
	c1 = 0
	c2 = 0
	c3 = 0
	c4 = 0
	# print(tab1)
	# print(tab2)
	# print(tab3)
	# print(tab4)
	for c1 in range(len(tab1)):
		if c1 % 100 == 0:
			print(c1)
		for c2 in range(len(tab2)):
			if (tab1[c1][CT_FILENAME] != tab2[c2][CT_FILENAME]): continue  # not same file
			# if (tab1[c1][CT_PART] != tab2[c2][CT_PART]): continue # not same participant
			#if (tab2[c2][CT_END] < tab1[c1][CT_BEGIN]): continue
			#if (tab2[c2][CT_BEGIN] > tab1[c1][CT_END]): break
			for c3 in range(len(tab3)):
				if (tab1[c1][CT_FILENAME] != tab3[c3][CT_FILENAME]): continue  # not same file
				# if (tab1[c1][CT_PART] != tab3[c3][CT_PART]): continue # not same participant
				#if (tab3[c3][CT_END] < tab2[c2][CT_BEGIN]): continue
				#if (tab3[c3][CT_BEGIN] > tab2[c2][CT_END]): break
				for c4 in range(len(tab4)):
					if (tab1[c1][CT_FILENAME] != tab4[c4][CT_FILENAME]): continue  # not same file
					# if (tab1[c1][CT_PART] != tab4[c4][CT_PART]): continue # not same participant
					#if (tab4[c4][CT_END] < tab3[c3][CT_BEGIN]): continue
					#if (tab4[c4][CT_BEGIN] > tab3[c3][CT_END]): break
					# memorize last starting comparison
					# print("lng:", tab1[c1], tab2[c2], tab3[c3], tab3[c4])
					begin = max(tab1[c1][CT_BEGIN], tab2[c2][CT_BEGIN], tab3[c3][CT_BEGIN], tab4[c4][CT_BEGIN])
					end = min(tab1[c1][CT_END], tab2[c2][CT_END], tab3[c3][CT_END], tab4[c4][CT_END])
					# print("mm:", begin, end)
					dur1 = tab1[c1][CT_END] - tab1[c1][CT_BEGIN]
					dur2 = tab2[c2][CT_END] - tab2[c2][CT_BEGIN]
					dur3 = tab3[c3][CT_END] - tab3[c3][CT_BEGIN]
					dur4 = tab4[c4][CT_END] - tab4[c4][CT_BEGIN]
					durinter = end - begin
					if (begin < end):
						result.append((
							tab1[c1][CT_ANNOT], tab1[c1][CT_BEGIN], tab1[c1][CT_END], tab1[c1][CT_DUR],
							tab1[c1][CT_TIER], tab1[c1][CT_TYPE], tab1[c1][CT_PART],
							tab2[c2][CT_ANNOT], tab2[c2][CT_BEGIN], tab2[c2][CT_END], tab2[c2][CT_DUR],
							tab2[c2][CT_TIER], tab2[c2][CT_TYPE], tab2[c2][CT_PART],
							tab3[c3][CT_ANNOT], tab3[c3][CT_BEGIN], tab3[c3][CT_END], tab3[c3][CT_DUR],
							tab3[c3][CT_TIER], tab3[c3][CT_TYPE], tab3[c3][CT_PART],
							tab4[c4][CT_ANNOT], tab4[c4][CT_BEGIN], tab4[c4][CT_END], tab4[c4][CT_DUR],
							tab4[c4][CT_TIER], tab4[c4][CT_TYPE], tab4[c4][CT_PART],
							begin, end, durinter, tab1[c1][CT_FILENAME]))
	return result


def intersection5simple(tab1, tab2, tab3, tab4, tab5):
	result = []
	c1 = 0
	c2 = 0
	c3 = 0
	c4 = 0
	c5 = 0
	# print(tab1)
	# print(tab2)
	# print(tab3)
	# print(tab4)
	# print(tab5)
	for c1 in range(len(tab1)):
		for c2 in range(len(tab2)):
			if (tab1[c1][CT_FILENAME] != tab2[c2][CT_FILENAME]): continue  # not same file
			# if (tab1[c1][CT_PART] != tab2[c2][CT_PART]): continue # not same participant
			#if (tab2[c2][CT_END] < tab1[c1][CT_BEGIN]): continue
			#if (tab2[c2][CT_BEGIN] > tab1[c1][CT_END]): break
			for c3 in range(len(tab3)):
				if (tab1[c1][CT_FILENAME] != tab3[c3][CT_FILENAME]): continue  # not same file
				# if (tab1[c1][CT_PART] != tab3[c3][CT_PART]): continue # not same participant
				#if (tab3[c3][CT_END] < tab2[c2][CT_BEGIN]): continue
				#if (tab3[c3][CT_BEGIN] > tab2[c2][CT_END]): break
				for c4 in range(len(tab4)):
					if (tab1[c1][CT_FILENAME] != tab4[c4][CT_FILENAME]): continue  # not same file
					# if (tab1[c1][CT_PART] != tab4[c4][CT_PART]): continue # not same participant
					#if (tab4[c4][CT_END] < tab3[c3][CT_BEGIN]): continue
					#if (tab4[c4][CT_BEGIN] > tab3[c3][CT_END]): break
					for c5 in range(len(tab5)):
						if (tab1[c1][CT_FILENAME] != tab5[c5][CT_FILENAME]): continue  # not same file
						# if (tab1[c1][CT_PART] != tab4[c5][CT_PART]): continue # not same participant
						#if (tab5[c5][CT_END] < tab4[c4][CT_BEGIN]): continue
						#if (tab5[c5][CT_BEGIN] > tab4[c4][CT_END]): break
						# memorize last starting comparison
						# print("lng:", tab1[c1], tab2[c2], tab3[c3], tab3[c4])
						begin = max(tab1[c1][CT_BEGIN], tab2[c2][CT_BEGIN], tab3[c3][CT_BEGIN], tab4[c4][CT_BEGIN],
									tab5[c5][CT_BEGIN])
						end = min(tab1[c1][CT_END], tab2[c2][CT_END], tab3[c3][CT_END], tab4[c4][CT_END],
								  tab5[c5][CT_BEGIN])
						# print("mm:", begin, end)
						dur1 = tab1[c1][CT_END] - tab1[c1][CT_BEGIN]
						dur2 = tab2[c2][CT_END] - tab2[c2][CT_BEGIN]
						dur3 = tab3[c3][CT_END] - tab3[c3][CT_BEGIN]
						dur4 = tab4[c4][CT_END] - tab4[c4][CT_BEGIN]
						dur4 = tab5[c5][CT_END] - tab5[c5][CT_BEGIN]
						durinter = end - begin
						if (begin < end):
							result.append((
								tab1[c1][CT_ANNOT], tab1[c1][CT_BEGIN], tab1[c1][CT_END], tab1[c1][CT_DUR],
								tab1[c1][CT_TIER], tab1[c1][CT_TYPE], tab1[c1][CT_PART],
								tab2[c2][CT_ANNOT], tab2[c2][CT_BEGIN], tab2[c2][CT_END], tab2[c2][CT_DUR],
								tab2[c2][CT_TIER], tab2[c2][CT_TYPE], tab2[c2][CT_PART],
								tab3[c3][CT_ANNOT], tab3[c3][CT_BEGIN], tab3[c3][CT_END], tab3[c3][CT_DUR],
								tab3[c3][CT_TIER], tab3[c3][CT_TYPE], tab3[c3][CT_PART],
								tab4[c4][CT_ANNOT], tab4[c4][CT_BEGIN], tab4[c4][CT_END], tab4[c4][CT_DUR],
								tab4[c4][CT_TIER], tab4[c4][CT_TYPE], tab4[c4][CT_PART],
								tab4[c5][CT_ANNOT], tab5[c5][CT_BEGIN], tab5[c5][CT_END], tab5[c5][CT_DUR],
								tab5[c5][CT_TIER], tab5[c5][CT_TYPE], tab5[c5][CT_PART],
								begin, end, durinter, tab1[c1][CT_FILENAME]))
	return result


def sort_and_fill(tier_content, eaf_end):
	tier_sorted = []
	if (len(tier_content) < 1):
		tier_sorted.append((0, eaf_end, "--"))
		return tier_sorted
	nl = np.argsort([x[0] for x in tier_content])
	# tri par rapport au temps de début et ajout des éléments vides
	# for i in nl:
	# 	tier_sorted.append(tier_content[i])
	# first element
	if (tier_content[nl[0]][0] > 0):
		# the fist element does not start at zero
		tier_sorted.append((0, tier_content[nl[0]][0], "--"))
	# add the first element
	tier_sorted.append(tier_content[nl[0]])
	# note the end of last element added
	last_end = tier_content[nl[0]][1]
	# now list all other elements
	for i in range(1, len(nl)):
		if (tier_content[nl[i]][0] > last_end):
			# there is a gap, add an element
			tier_sorted.append((last_end, tier_content[nl[i]][0], "--"))
		# add the current element
		tier_sorted.append(tier_content[nl[i]])
		last_end = tier_content[nl[i]][1]
	# end do we add a last element ?
	if (last_end < eaf_end):
		tier_sorted.append((last_end, eaf_end, "--"))
	return tier_sorted


def sort_first_column(tier_content, column=0):
	tier_sorted = []
	nl = np.argsort([x[column] for x in tier_content])
	for i in range(0, len(nl)):
		# add the current element
		tier_sorted.append(tier_content[nl[i]])
	return tier_sorted


def sort_two_columns(tier_content, col1, col2):
	tier_content.sort(key=operator.itemgetter(col1, col2))
	return tier_content


def sort_second_and_third_column(tier_content):
	return sort_two_columns(tier_content, 1, 2)
# tier_sorted = []
# nl = np.argsort([(x[1], x[2]) for x in tier_content])
# for i in range(0, len(nl)):
# 	# add the current element
# 	tier_sorted.append(tier_content[nl[i]])
# return tier_sorted
