
# /usr/bin/python3 ../src/intersections-main.py ../samples/DL-FRA1-DIN1-Complet-01-07-2022.eaf -o ../csvs/DL-example.xlsx -f xlsx
# /usr/bin/python3 ../src/intersections-main.py ../samples/DL-FRA1-DIN1-Complet-01-07-2022.eaf -o ../csvs/DL-example.csv
# /usr/bin/python3 ../src/intersections-main.py ../samples/DL-FRA1-DIN1-Complet-01-07-2022.eaf
# /usr/bin/python3 ../src/intersections-main.py ../samples/DL-FRA1-DIN1-Complet-01-07-2022.eaf -o ../csvs/aa.csv -c
# /usr/bin/python3 ../src/intersections-main.py ../samples/SF-F2-DIN1-Complet-01-07-2022-Extend.eaf -o ../csvs/aa.csv -c

import elanquery.intersections
import filecmp

def test_1():
	elanquery.intersections.compute_intersections(['../../data/samples/DL-FRA1-DIN1-Complet-01-07-2022.eaf', '-o', '../../data/csvs/test_1.csv'])
	assert filecmp.cmp('../../data/csvs/test_1.csv', '../../data/modeles/test_1.csv', shallow=False) == True

if __name__ == "__main__":
   test_1()
